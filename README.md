library
=======

## Эпиграф ##

Ниже о том, как развернуть приложение на локальной машине и немного о функционале. Разрабатывалось на системе с php 5.5.

## Установка ##

* cd /var/web/www
* git clone https://vitalik-kustov@bitbucket.org/vitalik-kustov/library.git
* cd library
* composer install -o

Далее система будет спрашивать различные параметры. Можно оставлять всё по умолчанию, но, возможно, Вы захотите изменить настройки подключения к базе данных. Осталось совсем чуть-чуть. Создать базу, накатить миграции и фикстуры, сдампить ассетики. Для этого выполнить следующие команды

* php app/console doctrine:database:create -e=prod
* echo "Y"|php app/console doctrine:migrations:migrate
* echo "Y"|php app/console doctrine:fixtures:load
* php app/console assetic:dump -e=prod

Настройки виртуального хоста

```
#!config

<VirtualHost library.loc:80>
    RewriteEngine On
    RewriteCond %{HTTP:Authorization} ^(.*)
    RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]
	
    DocumentRoot "/var/web/www/library/web"
    ServerName library.loc
    DirectoryIndex app.php
    ErrorLog "logs/library.loc-error.log"
    CustomLog "logs/library.loc-access.log" combined	
	<Directory "/var/web/www/library/web">
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>	
</VirtualHost>
```

Запускаем apache, открываем [http://library.loc/](http://library.loc/). Все готово.

## Использование ##

В базе будут созданы  

* 1 библиотекарь (ROLE_LIBRARIAN) admin:admin с ролью ROLE_API;  
* 1 библиотекарь (ROLE_LIBRARIAN) moder:moder  
* много читателей (ROLE_READER) test1:test1, test2:test2 ...  


### Раздел для библиотекарей ###

/admin

* Readers - панель для добавления/удаления/просмотра списка читателей

* Orders - панель для управления заказами и позициями заказа.

Параметры отображения списка

/admin/orders/list/{owner}/{state}/{expired}

    owner - (all|my)
    all - по умолчанию. Все заказы.
    my - только созданные текущим библиотекарем

    state - (all|active|closed)
    all - все заказы
    active - заказы в которых есть хотя бы одна незакрытая позиция
    closed - заказы в которых есть хотя бы одна закрытая позиция
    
    expired - (all|expired)
    all - все заказы
    expired - заказы, в которых есть "просроченные" позиции

* Deptors - список должников с количеством просроченных книг (позиций заказов)
* logout - разлогинеться

Для удобства сделал и регистрацию библиотекарей - /admin/register

### Раздел для читателей ###

/library - на этой странице читатель видит информацию о всех своих заказах, библиотекарь видит информацию обо всех созданных им заказах
Если зайти незалогиненным, то будет предложено залогинеться или зарегистрироваться.

### API ###

Документация по вызовам доступна по **[http://library.loc/api/doc](http://library.loc/api/doc)**, там же можно поиграться с вызовами, но перед этим нужно будет авторизоваться - отправить POST /api/login_check (полезная штука, на всякий случай - https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm ).
Как происходит авторизация можно посмотреть в файле теста, который написан с этой целью

Про роли.

В системе 3 основные роли:

* ROLE_API - имеет доступ ко всем методам API
* ROLE_LIBRARIAN - имеет доступ к безопасным методам API и админке
* ROLE_READER - имеет доступ к /library - информации о своих заказах


### Как использовать API ###

1. Получить token, отправив запрос POST /api/login_check с данными username и password
2. При каждом вызове API добавить token в http заголовок: Authorization: Bearer {token} или GET параметром: /api/v1/readers.json?limit=10&offset=20&bearer={token}

По любым вопросам доступен.

skype: atzal1k4  
тел: 8 923 41 55 045  
email: kustov.vitalik@gmail.com