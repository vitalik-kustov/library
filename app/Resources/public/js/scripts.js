$(function() {

	function addTagForm($collectionHolder, $newLinkLi) {
		var prototype = $collectionHolder.data('prototype');
		var index = $collectionHolder.data('index');
		var newForm = prototype.replace(/__name__/g, index);
		$collectionHolder.data('index', index + 1);
		var $newFormLi = $('<li></li>').append(newForm);
		$newLinkLi.before($newFormLi);
	}

	var $collectionHolder;
	var $addTagLink = $('<a href="#" class="add_tag_link">Add an item</a>');
	var $newLinkLi = $('<li></li>').append($addTagLink);

	$collectionHolder = $('ul.items');
	$collectionHolder.append($newLinkLi);
	$collectionHolder.data('index', $collectionHolder.find(':input').length);
	$addTagLink.on('click', function(e) {
		e.preventDefault();
		addTagForm($collectionHolder, $newLinkLi);
	});

	$('ul.items').on('click', '.kill-btn', function(e) {
		e.preventDefault();
		$(this).closest('li').remove();
	});
});