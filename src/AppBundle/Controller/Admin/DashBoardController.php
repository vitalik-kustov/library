<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Repository\ReaderRepositoryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DashBoardController
 * @Route("/admin", service="app.controller.admin.dashboard")
 * @package AppBundle\Controller\Admin
 */
class DashBoardController extends Controller
{

	/**
	 * @var PaginatorInterface
	 */
	private $paginator;

	/**
	 * @var ReaderRepositoryInterface
	 */
	private $readerRepository;

	/**
	 * @var int
	 */
	private $itemsPerPage;

	/**
	 * @param PaginatorInterface $paginator
	 * @param ReaderRepositoryInterface $readerRepository
	 * @param $itemsPerPage
	 */
	public function __construct(
		PaginatorInterface $paginator,
		ReaderRepositoryInterface $readerRepository,
		$itemsPerPage
	)
	{
		$this->paginator = $paginator;
		$this->readerRepository = $readerRepository;
		$this->itemsPerPage = $itemsPerPage;
	}

	/**
	 * @Route("/dashboard", name="admin_dashboard")
	 */
	public function indexAction(Request $request)
	{
		return $this->render(':admin/dashboard:index.html.twig', [
			'pagination' => $this->paginator->paginate(
				$this->readerRepository->findDeptorsWithDebtCount(),
				$request->query->getInt('page', 1),
				$this->itemsPerPage
			),
		]);
	}

}