<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Order;
use AppBundle\Form\Type\OrderFormType;
use AppBundle\Repository\OrderRepositoryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OrdersController
 * @Route("/admin/orders", service="app.controller.admin.orders")
 * @package AppBundle\Controller\Admin
 */
class OrdersController extends Controller
{

	/**
	 * @var PaginatorInterface
	 */
	private $paginator;

	/**
	 * @var OrderRepositoryInterface
	 */
	private $orderRepository;

	/**
	 * @var int
	 */
	private $itemsPerPage;

	/**
	 * @param PaginatorInterface $paginator
	 * @param OrderRepositoryInterface $orderRepository
	 * @param $itemsPerPage
	 */
	public function __construct(
		PaginatorInterface $paginator,
		OrderRepositoryInterface $orderRepository,
		$itemsPerPage
	)
	{
		$this->paginator = $paginator;
		$this->orderRepository = $orderRepository;
		$this->itemsPerPage = $itemsPerPage;
	}


	/**
	 * @Route("/list/{owner}/{state}/{expired}", name="admin_orders_list", defaults={"owner"="all", "state"="all", "expired"="all"}, requirements={"owner"="(all|my)", "state" = "(all|active|closed)", "expired"="(all|expired)"})
	 * @param Request $request
	 * @param $owner
	 * @param $state
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function listAction(Request $request, $owner, $state, $expired)
	{
		return $this->render(':admin/orders:list.hmtl.twig', [
			'pagination' => $this->paginator->paginate(
				$this->orderRepository->findLibrarianFilteredOrders($owner, $state, $expired),
				$request->query->getInt('page', 1),
				$this->itemsPerPage
			),
		]);
	}

	/**
	 * @Route("/create", name="admin_orders_create")
	 */
	public function createAction()
	{
		$order = new Order();
		$form = $this->createForm(new OrderFormType(), $order, [
			'action' => $this->generateUrl('admin_orders_create')
		]);

		$process = $this->processForm($form);
		if ($process) {
			return $this->redirectToRoute('admin_orders_list');
		}

		return $this->render(':admin/orders:form.html.twig', [
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/edit/{id}", name="admin_orders_edit")
	 */
	public function editAction(Order $order, Request $request)
	{
		$form = $this->createForm(new OrderFormType(), $order, [
			'action' => $this->generateUrl('admin_orders_edit', ['id' => $order->getId()])
		]);
		$process = $this->processForm($form);
		if ($process) {
			return $this->redirectToRoute('admin_orders_list');
		}

		return $this->render(':admin/orders:form.html.twig', [
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Route("/delete/{id}", name="admin_orders_delete")
	 */
	public function deleteAction(Order $order)
	{
		$this->orderRepository->deleteOrder($order);

		return $this->redirectToRoute('admin_orders_list');
	}

	/**
	 * @param Form $form
	 *
	 * @return bool
	 * @internal param Order $order
	 */
	private function processForm(Form $form)
	{
		$request = $this->get('request');
		if ($request->getMethod() === Request::METHOD_POST) {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$this->orderRepository->saveOrder($form->getData());

				return true;
			}
		}

		return false;
	}

}