<?php

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Reader;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Form\Handler\RegistrationFormHandler;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiReaderController
 * @Route("/admin/readers", service="app.controller.admin.readers")
 * @package AppBundle\Controller\Admin
 */
class ReadersController extends Controller
{

	/**
	 * @var PaginatorInterface
	 */
	private $paginator;

	/**
	 * @var UserManager
	 */
	private $userProvider;

	/**
	 * @var int
	 */
	private $itemsPerPage;

	/**
	 * @var Form
	 */
	private $registrationForm;

	/**
	 * @var RegistrationFormHandler
	 */
	private $registrationFormHandler;

	/**
	 * @param PaginatorInterface $paginator
	 * @param UserManager $userProvider
	 * @param $itemsPerPage
	 * @param Form $registrationForm
	 * @param RegistrationFormHandler $registrationFormHandler
	 */
	public function __construct(
		PaginatorInterface $paginator,
		UserManager $userProvider,
		$itemsPerPage,
		Form $registrationForm,
		RegistrationFormHandler $registrationFormHandler
	)
	{
		$this->paginator = $paginator;
		$this->userProvider = $userProvider;
		$this->itemsPerPage = $itemsPerPage;
		$this->registrationForm = $registrationForm;
		$this->registrationFormHandler = $registrationFormHandler;
	}


	/**
	 * @Route("/list", name="admin_readers_list")
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function listAction(Request $request)
	{
		return $this->render(':admin/readers:list.html.twig', [
			'pagination' => $this->paginator->paginate(
				$this->userProvider->findUsers(),
				$request->query->getInt('page', 1),
				$this->itemsPerPage
			),
		]);
	}

	/**
	 * @Route("/delete/{id}", name="admin_readers_delete")
	 * @param Reader $reader
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function deleteAction(Reader $reader)
	{
		$this->userProvider->deleteUser($reader);

		return $this->redirectToRoute('admin_readers_list');
	}

	/**
	 * @Route("/create", name="admin_readers_create")
	 */
	public function create()
	{
		if ($this->registrationFormHandler->process(false)) {
			return $this->redirectToRoute('admin_readers_list');
		}

		return $this->render(':admin/readers:create.html.twig', [
			'form' => $this->registrationForm->createView()
		]);
	}

}