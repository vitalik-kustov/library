<?php

namespace AppBundle\Controller\Admin;


use FOS\UserBundle\Controller\RegistrationController as BaseRegistrationController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegistrationController extends BaseRegistrationController
{
	public function registerAction()
	{
		$form = $this->container->get('app.registration_form');
		$formHandler = $this->container->get('app.registration_form_handler.default');
		$confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

		$process = $formHandler->process($confirmationEnabled);
		if ($process) {
			$user = $form->getData();

			$authUser = false;
			if ($confirmationEnabled) {
				$this->container->get('session')
					->set('fos_user_send_confirmation_email/email', $user->getEmail());
				$route = 'fos_user_registration_check_email';
			} else {
				$authUser = true;
				$route = 'fos_user_registration_confirmed';
			}

			$this->setFlash('fos_user_success', 'registration.flash.user_created');
			$url = $this->container->get('router')
				->generate($route);
			$response = new RedirectResponse($url);

			if ($authUser) {
				$this->authenticateUser($user, $response);
			}

			return $response;
		}

		return $this->container->get('templating')
			->renderResponse(':admin/register:register.html.twig', array(
				'form' => $form->createView(),
			));
	}


}