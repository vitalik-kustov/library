<?php

namespace AppBundle\Controller\Admin;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class SecurityController extends Controller
{

	public function loginAction()
	{
		$authUtils = $this->container->get('security.authentication_utils');

		$error = $authUtils->getLastAuthenticationError();
		if ($error) {
			$error = $error->getMessage();
		}

		$lastUsername = $authUtils->getLastUsername();

		return $this->render(':admin/security:login.html.twig', [
			'last_username' => $lastUsername,
			'error' => $error,
		]);
	}

	public function checkAction()
	{
		throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
	}

	public function logoutAction()
	{
		throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
	}

}