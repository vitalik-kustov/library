<?php

namespace AppBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ApiDeptorsController extends FOSRestController {

	/**
	 * Get deptors
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      200 = "Returns when successful"
	 *  }
	 * )
	 *
	 * @REST\View()
	 *
	 * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Deptors collection limit")
	 * @REST\QueryParam(name="offset", requirements="\d+", nullable=true, description="Deptors collection offset")
	 *
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 *
	 * @param ParamFetcherInterface $fetcher
	 *
	 * @return \AppBundle\Entity\DeptorsCollection
	 */
	public function getDeptorsAction(ParamFetcherInterface $fetcher)
	{
		$limit = (int)$fetcher->get('limit');
		$offset = $fetcher->get('offset');
		$offset = ($offset == null) ? 0 : (int)$offset;
		return $this->container->get('app.rest_handler.deptor')->all($limit, $offset);
	}

}