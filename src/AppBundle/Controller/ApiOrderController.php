<?php

namespace AppBundle\Controller;

use AppBundle\Exception\InvalidFormException;
use AppBundle\Form\Type\OrderType;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ApiOrderController
 * @package AppBundle\Controller
 */
class ApiOrderController extends FOSRestController
{

	/**
	 * Get reader orders
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      200 = "Returned when successful"
	 *  }
	 * )
	 * @REST\View(templateVar="orders")
	 * @REST\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset collection parameter")
	 * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Limit collection parameter")
	 * @REST\QueryParam(name="sortBy", requirements="(createdAt|updatedAt|id)", default="id", description="Sort field")
	 * @REST\QueryParam(name="sortType", requirements="(ASC|DESC)", default="ASC", description="Sort type")
	 *
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 *
	 * @param int $readerId reader id
	 *
	 * @return array
	 */
	public function getOrdersAction($readerId, ParamFetcherInterface $fetcher)
	{
		$offset = $fetcher->get('offset');
		$offset = ($offset == null) ? 0 : (int)$offset;
		$limit = (int)$fetcher->get('limit');
		$sortBy = $fetcher->get('sortBy');
		$sortType = $fetcher->get('sortType');

		return $this->container->get('app.rest_handler.order')
			->all(
				$readerId, $limit, $offset, $sortBy, $sortType
			);
	}

	/**
	 * Get single order
	 *
	 * @param int $readerId reader id
	 * @param int $id order id
	 *
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 * @return \FOS\RestBundle\View\View
	 * @REST\View(templateVar="order")
	 * @ApiDoc(
	 *  resource=true,
	 *  output="AppBundle\Entity\Order",
	 *  statusCodes={
	 *      200 = "Returned when successful",
	 *      404 = "Returned when the order is not found"
	 *  }
	 * )
	 */
	public function getOrderAction($readerId, $id)
	{
		return $this->getOrderOr404($readerId, $id);
	}

	/**
	 * Presents the form to use to create a new order
	 * @return \Symfony\Component\Form\Form
	 * @REST\View()
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      200 = "Returned when successful"
	 *  }
	 * )
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param int $readerId reader id
	 */
	public function newOrderAction($readerId)
	{
		return $this->createForm(new OrderType());
	}

	/**
	 * Create a new order from submitted form data
	 * @REST\View(
	 *  statusCode=Codes::HTTP_BAD_REQUEST
	 * )
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Creates a new order from the submitted data.",
	 *  input="AppBundle\Form\Type\OrderType",
	 *  statusCodes={
	 *      201 = "Returned when successful",
	 *      400 = "Returned when the form has errors"
	 *  }
	 * )
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param int $readerId reader id
	 * @param Request $request request
	 *
	 * @return \Symfony\Component\Form\FormInterface
	 */
	public function postOrderAction($readerId, Request $request)
	{
		try {
			$newReader = $this->container->get('app.rest_handler.order')
				->post(
					$readerId,
					$request->request->all()
				);
			$routeOptions = array(
				'readerId' => $readerId,
				'id' => $newReader->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_1_get_reader_order', $routeOptions, Codes::HTTP_CREATED);
		} catch (InvalidFormException $e) {
			return $e->getForm();
		}
	}

	/**
	 * Update existing order from the submitted data or create a new order at a specific location
	 * @ApiDoc(
	 *  resource=true,
	 *  input="AppBundle\Form\Type\OrderType",
	 *  statusCodes={
	 *      201 = "Returned when the reader is created",
	 *      204 = "Returned when successful",
	 *      400 = "Returned when the form has errors"
	 *  }
	 * )
	 * @REST\View()
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param int $readerId reader id
	 * @param int $id order id
	 * @param Request $request request
	 *
	 * @return array|null
	 */
	public function putOrderAction($readerId, $id, Request $request)
	{
		try {
			if (!($order = $this->getOrderOr404($readerId, $id))) {
				$statusCode = Codes::HTTP_CREATED;
				$order = $this->container->get('app.rest_handler.order')
					->post(
						$readerId,
						$request->request->all()
					);
			} else {
				$statusCode = Codes::HTTP_NO_CONTENT;
				$order = $this->container->get('app.rest_handler.order')
					->put(
						$order,
						$readerId,
						$request->request->all()
					);
			}
			$routeOptions = array(
				'readerId' => $readerId,
				'id' => $order->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_1_get_reader_order', $routeOptions, $statusCode);
		} catch (InvalidFormException $e) {
			return $e->getForm();
		}
	}

	/**
	 * Update existing order from the submitted data or create a new order at a specific location.
	 * @ApiDoc(
	 *   resource = true,
	 *   input = "AppBundle\Form\Type\OrderType",
	 *   statusCodes = {
	 *     204 = "Returned when successful",
	 *     400 = "Returned when the form has errors"
	 *   }
	 * )
	 * @REST\View()
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param int $readerId reader id
	 * @param int $id the page id
	 * @param Request $request the request object
	 *
	 * @return View|FormTypeInterface
	 */
	public function patchOrderAction($readerId, $id, Request $request)
	{
		try {
			$order = $this->container->get('app.rest_handler.order')
				->patch(
					$this->getOrderOr404($readerId, $id),
					$readerId,
					$request->request->all()
				);
			$routeOptions = array(
				'readerId' => $readerId,
				'id' => $order->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_1_get_reader_order', $routeOptions, Codes::HTTP_NO_CONTENT);
		} catch (InvalidFormException $exception) {
			return $exception->getForm();
		}
	}

	/**
	 * Delete order
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Delete order with specified id",
	 *  statusCodes={
	 *      204 = "Returned when successful"
	 *  }
	 * )
	 * @REST\View(statusCode=204)
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param int $readerId reader id
	 * @param int $id reader id
	 */
	public function deleteOrderAction($readerId, $id)
	{
		$order = $this->getOrderOr404($readerId, $id);
		$this->container->get('app.rest_handler.order')
			->delete($order, $readerId);
	}

	private function getOrderOr404($readerId, $id)
	{
		$order = $this->container->get('app.rest_handler.order')
			->get($readerId, $id);
		if (!$order) {
			throw new NotFoundHttpException(sprintf('Order #%s for Reader #%s not found', $id, $readerId));
		}

		return $order;
	}
}