<?php

namespace AppBundle\Controller;


use AppBundle\Entity\OrderItem;
use AppBundle\Entity\OrderItemsCollection;
use AppBundle\Exception\InvalidFormException;
use AppBundle\Form\Type\OrderItemType;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ApiOrderItemController
 * @package AppBundle\Controller
 */
class ApiOrderItemController extends FOSRestController
{

	/**
	 * Get order items collection
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      200 = "Returned when successful"
	 *  }
	 * )
	 * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Collection limit")
	 * @REST\QueryParam(name="offset", requirements="\d+", nullable=true, description="Collection offset")
	 * @REST\QueryParam(name="sortBy", requirements="(id|createdAt|updatedAt|expiredAt)", default="id", description="Collection offset")
	 * @REST\QueryParam(name="sortType", requirements="(ASC|DESC)", default="ASC", description="Collection offset")
	 * @REST\View()
	 *
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 *
	 * @param int $readerId reader id
	 * @param int $orderId order id
	 * @param ParamFetcherInterface $fetcher fetcher
	 *
	 * @return OrderItemsCollection
	 */
	public function getItemsAction($readerId, $orderId, ParamFetcherInterface $fetcher)
	{
		$limit = (int)$fetcher->get('limit');
		$offset = $fetcher->get('offset');
		$offset = ($offset == null) ? 0 : (int)$offset;
		$sortBy = $fetcher->get('sortBy');
		$sortType = $fetcher->get('sortType');

		return $this->container->get('app.rest_handler.order_item')
			->all(
				$readerId, $orderId, $limit, $offset, $sortBy, $sortType
			);
	}

	/**
	 * Get a single order item
	 * @ApiDoc(
	 *  resource=true,
	 *  output="AppBundle\Entity\OrderItem",
	 *  statusCodes={
	 *      200 = "Returned when successful",
	 *      404 = "Returned when the order is not found"
	 *  }
	 * )
	 * @REST\View()
	 *
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param $id
	 *
	 * @return OrderItem
	 * @throws NotFoundHttpException
	 */
	public function getItemAction($readerId, $orderId, $id)
	{
		return $this->getItemOr404($readerId, $orderId, $id);
	}

	/**
	 * Presents the form to use to create a new order
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      200 = "Returned when successful"
	 *  }
	 * )
	 * @REST\View()
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param $readerId
	 * @param $orderId
	 *
	 * @return \Symfony\Component\Form\Form
	 */
	public function newItemAction($readerId, $orderId)
	{
		return $this->createForm(new OrderItemType());
	}

	/**
	 * Create a new order item from submitted form data
	 * @REST\View(
	 *  templateVar="form",
	 *  statusCode=Codes::HTTP_BAD_REQUEST
	 * )
	 * @ApiDoc(
	 *  resource=true,
	 *  input="AppBundle\Form\Type\OrderItemType",
	 *  statusCodes={
	 *      201 = "Returned when successful",
	 *      400 = "Returned when the form has errors"
	 *  }
	 * )
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param Request $request
	 *
	 * @return array|null|\Symfony\Component\HttpFoundation\RedirectResponse
	 * @throws \Exception
	 */
	public function postItemAction($readerId, $orderId, Request $request)
	{
		try {
			$item = $this->container->get('app.rest_handler.order_item')
				->post(
					$readerId, $orderId, $request->request->all()
				);

			$routeParams = [
				'readerId' => $readerId,
				'orderId' => $orderId,
				'id' => $item->getId(),
				'_format' => $request->get('_format')
			];

			return $this->routeRedirectView('api_1_get_reader_order_item', $routeParams, Codes::HTTP_CREATED);
		} catch (InvalidFormException $e) {
			return $e->getForm();
		}
	}

	/**
	 * Put an order item
	 * @ApiDoc(
	 *  resource=true,
	 *  input="AppBundle\Form\Type\OrderItemType",
	 *  statusCodes={
	 *      201 = "Returned when the reader is created",
	 *      204 = "Returned when successful",
	 *      400 = "Returned when the form has errors"
	 *  }
	 * )
	 * @REST\View()
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param $id
	 * @param Request $request
	 *
	 * @return array|\FOS\RestBundle\View\View|null
	 * @throws \Exception
	 */
	public function putItemAction($readerId, $orderId, $id, Request $request)
	{
		try {
			if (!($item = $this->getItemOr404($readerId, $orderId, $id))) {
				$item = $this->container->get('app.rest_handler.order_item')
					->post(
						$readerId,
						$orderId,
						$request->request->all()
					);
				$statusCode = Codes::HTTP_CREATED;
			} else {
				$item = $this->container->get('app.rest_handler.order_item')
					->put(
						$item,
						$readerId,
						$orderId,
						$request->request->all()
					);
				$statusCode = Codes::HTTP_NO_CONTENT;
			}
			$routeParams = [
				'readerId' => $readerId,
				'orderId' => $orderId,
				'id' => $item->getId(),
				'_format' => $request->get('_format')
			];

			return $this->routeRedirectView('api_1_get_reader_order_item', $routeParams, $statusCode);
		} catch (InvalidFormException $e) {
			return $e->getForm();
		}
	}

	/**
	 * Patch the order item
	 * @ApiDoc(
	 *  resource=true,
	 *  input="AppBundle\Form\Type\OrderItemType",
	 *  statusCodes={
	 *      204 = "Returned when successful",
	 *      400 = "Returned when the form has errors"
	 *  }
	 * )
	 * @REST\View()
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param $id
	 * @param Request $request
	 *
	 * @return array|\FOS\RestBundle\View\View|null
	 * @throws \Exception
	 */
	public function patchItemAction($readerId, $orderId, $id, Request $request)
	{
		try {
			$oldItem = $this->getItemOr404($readerId, $orderId, $id);
			dump($oldItem);
			$item = $this->container->get('app.rest_handler.order_item')
				->patch(
					$oldItem,
					$readerId,
					$orderId,
					$request->request->all()
				);
			$routeParams = [
				'id' => $item->getId(),
				'readerId' => $readerId,
				'orderId' => $orderId,
				'_format' => $request->get('_format')
			];

			return $this->routeRedirectView('api_1_get_reader_order_item', $routeParams, Codes::HTTP_NO_CONTENT);
		} catch (InvalidFormException $e) {
			return $e->getForm();
		}
	}

	/**
	 * Delete order item with specified id
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      204 = "Returned when successful"
	 *  }
	 * )
	 * @REST\View(statusCode=204)
	 *
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param $id
	 */
	public function deleteItemAction($readerId, $orderId, $id)
	{
		$item = $this->getItemOr404($readerId, $orderId, $id);
		$this->container->get('app.rest_handler.order_item')
			->delete($item);
	}

	/**
	 * @param $readerId
	 * @param $orderId
	 * @param $id
	 *
	 * @return OrderItem|null
	 * @throws NotFoundHttpException
	 */
	private function getItemOr404($readerId, $orderId, $id)
	{
		$item = $this->container->get('app.rest_handler.order_item')
			->get(
				$readerId, $orderId, $id
			);

		if (!$item) {
			throw new NotFoundHttpException('Order item not found');
		}

		return $item;
	}

}