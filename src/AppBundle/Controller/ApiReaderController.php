<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use AppBundle\Exception\InvalidFormException;
use AppBundle\Form\Type\ReaderType;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ApiReaderController
 * @package AppBundle\Controller\API
 */
class ApiReaderController extends FOSRestController
{
	/**
	 * List all readers
	 * @REST\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset collection parameter")
	 * @REST\QueryParam(name="limit", requirements="\d+", default="5", description="Limit collection parameter")
	 * @REST\QueryParam(name="sortBy", requirements="(id|username)", default="id", description="Sort field")
	 * @REST\QueryParam(name="sortType", requirements="(ASC|DESC)", default="ASC", description="Sort type")
	 * @REST\View()
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      200 = "Returned when successful"
	 *  }
	 * )
	 *
	 * @param ParamFetcherInterface $fetcher
	 *
	 * @return \AppBundle\Entity\ReadersCollection
	 */
	public function getReadersAction(ParamFetcherInterface $fetcher)
	{
		$offset = $fetcher->get('offset');
		$offset = ($offset == null) ? 0 : (int)$offset;
		$limit = (int)$fetcher->get('limit');
		$sortBy = $fetcher->get('sortBy');
		$sortType = $fetcher->get('sortType');

		return $this->container->get('app.rest_handler.reader')
			->all($limit, $offset, $sortBy, $sortType);
	}

	/**
	 * Get single reader
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Gets a reader for a given id",
	 *  output="AppBundle\Entity\Reader",
	 *  statusCodes={
	 *      200 = "Returned when successful",
	 *      404 = "Returned when the reader is not found"
	 *  }
	 * )
	 * @REST\View()
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 *
	 * @param $id
	 *
	 * @return \FOS\RestBundle\View\View
	 */
	public function getReaderAction($id)
	{
		return $this->getSingleOr404($id);
	}

	/**
	 * Presents the form to use to create a new reader
	 * @return \Symfony\Component\Form\Form
	 * @REST\View()
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *      200 = "Returned when successful"
	 *  }
	 * )
	 * @Security("has_role('ROLE_API')")
	 */
	public function newReaderAction()
	{
		return $this->createForm(new ReaderType());
	}

	/**
	 * Create a new reader from submitted form data
	 * @REST\View(
	 *  statusCode=Codes::HTTP_BAD_REQUEST
	 * )
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Creates a new reader from the submitted data.",
	 *  input="AppBundle\Form\Type\ReaderType",
	 *  statusCodes={
	 *      201 = "Returned when the reader is created",
	 *      400 = "Returned when the form has errors"
	 *  }
	 * )
	 * @Security("has_role('ROLE_API')")
	 * @return \Symfony\Component\Form\FormInterface
	 */
	public function postReaderAction(Request $request)
	{
		try {
			$newReader = $this->container->get('app.rest_handler.reader')
				->post(
					$request->request->all()
				);
			$routeOptions = array(
				'id' => $newReader->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_1_get_reader', $routeOptions, Codes::HTTP_CREATED);
		} catch (InvalidFormException $e) {
			return $e->getForm();
		}
	}

	/**
	 * Update existing reader from the submitted data or create a new reader at a specific location
	 * @ApiDoc(
	 *  resource=true,
	 *  input="AppBundle\Form\Type\ReaderType",
	 *  statusCodes={
	 *      201 = "Returned when the reader is created",
	 *      204 = "Returned when successful",
	 *      400 = "Returned when the form has errors"
	 *  }
	 * )
	 * @REST\View()
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param Request $request
	 * @param $id
	 *
	 * @return array|null
	 */
	public function putReaderAction(Request $request, $id)
	{
		try {
			if (!($reader = $this->getSingleOr404($id))) {
				$statusCode = Codes::HTTP_CREATED;
				$reader = $this->container->get('app.rest_handler.reader')
					->post(
						$request->request->all()
					);
			} else {
				$statusCode = Codes::HTTP_NO_CONTENT;
				$reader = $this->container->get('app.rest_handler.reader')
					->put(
						$reader,
						$request->request->all()
					);
			}
			$routeOptions = array(
				'id' => $reader->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_1_get_reader', $routeOptions, $statusCode);
		} catch (InvalidFormException $e) {
			return $e->getForm();
		}
	}

	/**
	 * Update existing reader from the submitted data or create a new reader at a specific location.
	 * @ApiDoc(
	 *   resource = true,
	 *   input = "AppBundle\Form\Type\ReaderType",
	 *   statusCodes = {
	 *     204 = "Returned when successful",
	 *     400 = "Returned when the form has errors"
	 *   }
	 * )
	 * @REST\View()
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param Request $request the request object
	 * @param int $id the page id
	 *
	 * @return FormTypeInterface|View
	 * @throws NotFoundHttpException when page not exist
	 */
	public function patchReaderAction(Request $request, $id)
	{
		try {
			$reader = $this->container->get('app.rest_handler.reader')
				->patch(
					$this->getSingleOr404($id),
					$request->request->all()
				);
			$routeOptions = array(
				'id' => $reader->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_1_get_reader', $routeOptions, Codes::HTTP_NO_CONTENT);
		} catch (InvalidFormException $exception) {
			return $exception->getForm();
		}
	}

	/**
	 * Delete reader
	 * @ApiDoc(
	 *  resource=true,
	 *  description="Delete reader with specified id",
	 *  statusCodes={
	 *      204 = "Returned when successful"
	 *  }
	 * )
	 * @REST\View(statusCode=204)
	 * @Security("has_role('ROLE_API')")
	 *
	 * @param int $id reader id
	 */
	public function deleteReaderAction($id)
	{
		$resder = $this->getSingleOr404($id);
		$this->container->get('app.rest_handler.reader')
			->delete($resder);
	}

	/**
	 * @param $id
	 *
	 * @return Reader
	 */
	private function getSingleOr404($id)
	{
		$reader = $this->get('app.rest_handler.reader')
			->getSingle($id);
		if (!$reader) {
			throw new NotFoundHttpException(sprintf('Reader #%s not found', $id));
		}

		return $reader;
	}

}