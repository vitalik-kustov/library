<?php

namespace AppBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as REST;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ApiTop10AuthorController extends FOSRestController {

	/**
	 * Get Top 10 authors in the period
	 *
	 * @param ParamFetcherInterface $fetcher
	 *
	 * @REST\QueryParam(name="startDate", nullable=false, requirements="^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})?", default="2010-01-01 00:00:00", description="Stats date from")
	 * @REST\QueryParam(name="finishDate", nullable=false, requirements="^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})?", default="2020-01-01 00:00:00", description="Stats date to")
	 *
	 * @ApiDoc(
	 *  resource=true,
	 *  statusCodes={
	 *		200 = "Return when successful"
	 *  }
	 * )
	 *
	 * @REST\View();
	 *
	 * @Security("has_role('ROLE_LIBRARIAN') or has_role('ROLE_API')")
	 *
	 * @return mixed
	 */
	public function getTop10authorsAction(ParamFetcherInterface $fetcher)
	{
		$startDate = new \DateTime($fetcher->get('startDate'));
		$finishDate = new \DateTime($fetcher->get('finishDate'));
		return $this->container->get('app.repository.author')->findTop10Authors($startDate, $finishDate);
	}

}