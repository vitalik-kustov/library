<?php

namespace AppBundle\Controller\Library;

use KustovVitalik\MaintenanceBundle\Annotations\Maintenance;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController
 * @package AppBundle\Controller\Library
 */
class DefaultController extends Controller
{
	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction()
	{
		return $this->render('default/index.html.twig');
	}

	/**
	 * @Route("/library", name="library")
	 */
	public function libraryAction()
	{
		return $this->render(':default:library.html.twig');
	}
}
