<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Author;
use AppBundle\Entity\Book;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBookAuthorsData implements FixtureInterface, OrderedFixtureInterface
{

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager)
	{
		$books = $this->createBooks($manager);
		$authors = $this->createAuthors($manager);

		/** @var Book $book */
		foreach ($books as $book) {

			$authorsCount = floor(rand(1, 4));
			$counter = 0;

			/** @var Author $author */
			shuffle($authors);
			foreach ($authors as $author) {
				if ($counter < $authorsCount) {
					$book->addAuthor($author);
					$manager->persist($book);
					$counter++;
				}
			}
		}
		$manager->flush();

	}

	private function createBooks(ObjectManager $manager)
	{
		$books = [];
		for ($i = 1; $i < 31; $i++) {
			$book = new Book();
			$book->setName('Book #' . $i);
			$manager->persist($book);
			array_push($books, $book);
		}
		$manager->flush();

		return $books;
	}

	private function createAuthors(ObjectManager $manager)
	{
		$authors = [];
		for ($i = 1; $i < 11; $i++) {
			$author = new Author();
			$author->setName('Author #' . $i);
			$manager->persist($author);
			array_push($authors, $author);
		}
		$manager->flush();

		return $authors;
	}

	/**
	 * Get the order of this fixture
	 * @return integer
	 */
	public function getOrder()
	{
		return 0;
	}
}