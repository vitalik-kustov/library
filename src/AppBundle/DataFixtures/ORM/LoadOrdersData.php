<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 08.06.2015
 * Time: 19:30
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Reader;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadOrdersData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * Sets the Container.
	 *
	 * @param ContainerInterface|null $container A ContainerInterface instance or null
	 *
	 * @api
	 */
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager)
	{
		$readers = $this->container->get('app.repository.reader')->findAll();
		$librarians = $this->container->get('app.repository.librarian')->findAll();
		$books = $this->container->get('app.repository.book')->findAll();


		$em = $this->container->get('doctrine.orm.entity_manager');

		$bools = [true, false];
		$randomDate = function() use ($bools) {
			$int = floor(rand(3, 30));
			if ($bools[array_rand($bools)]) {
				return new \DateTime('+ ' . $int * 4 . ' days');
			} else {
				return new \DateTime('- ' . $int * 4 . ' days');
			}
		};

		if (count($readers)) {
			/** @var Reader $reader */
			foreach($readers as $reader) {

				$countOrders = floor(rand(1, 10));

				while($countOrders > 0) {
					$order = new Order();
					$order->setReader($reader);
					$order->setLibrarian($librarians[array_rand($librarians)]);

					$orderItemCount = floor(rand(1, 5));
					while($orderItemCount > 0) {
						$item = new OrderItem();
						$item->setBook($books[array_rand($books)]);
						$item->setOrder($order);
						$item->setClosed($bools[array_rand($bools)]);
						$item->setExpiredAt($randomDate());
						$order->addItem($item);
						$em->persist($item);
						$orderItemCount--;
					}
					$em->persist($order);
					$reader->addOrder($order);
					$countOrders--;
				}

				$em->persist($reader);
			}

			$em->flush();
		}

	}

	/**
	 * Get the order of this fixture
	 * @return integer
	 */
	public function getOrder()
	{
		return 200;
	}

}