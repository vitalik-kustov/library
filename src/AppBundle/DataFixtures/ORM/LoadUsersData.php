<?php

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsersData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager)
	{
		$userManager = $this->container->get('app.provider.reader');
		for ($i = 1; $i < 30; $i++) {
			$reader = $userManager->createUser();
			$reader->setUsername('test' . $i);
			$reader->setEmail('test' . $i . '@test.com');
			$reader->setPlainPassword('test' . $i);
			$reader->setEnabled(true);
			$userManager->updatePassword($reader);
			$userManager->updateUser($reader);
		}

		$librarianManager = $this->container->get('app.provider.librarian');
		$admin = $librarianManager->createUser();
		$admin->setUsername('admin');
		$admin->setEmail('admin@admin.com');
		$admin->setPlainPassword('admin');
		$admin->setEnabled(true);
		$admin->addRole('ROLE_API');
		$librarianManager->updatePassword($admin);
		$librarianManager->updateUser($admin);

		$moderator = $librarianManager->createUser();
		$moderator->setUsername('moder');
		$moderator->setEmail('moder@moder.com');
		$moderator->setPlainPassword('moder');
		$moderator->setEnabled(true);
		$librarianManager->updatePassword($moderator);
		$librarianManager->updateUser($moderator);
	}

	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * Sets the Container.
	 *
	 * @param ContainerInterface|null $container A ContainerInterface instance or null
	 *
	 * @api
	 */
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}

	/**
	 * Get the order of this fixture
	 * @return integer
	 */
	public function getOrder()
	{
		return 100;
	}

}