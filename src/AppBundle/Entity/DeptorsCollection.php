<?php

namespace AppBundle\Entity;

/**
 * Class DeptorsCollection
 * @package AppBundle\Entity
 */
class DeptorsCollection {

	public $deptors;

	public $limit;

	public $offset;

	public function __construct($deptors, $limit, $offset)
	{
		$this->deptors = $deptors;
		$this->limit = $limit;
		$this->offset = $offset;
	}
}