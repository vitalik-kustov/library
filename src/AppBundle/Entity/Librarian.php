<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Symfony\Component\Security\Core\Role\Role;

/**
 * Class Librarian
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LibrarianRepository")
 * @ORM\Table(name="librarians")
 * @package AppBundle\Entity
 */
class Librarian extends BaseUser
{

	/**
	 * @var int
	 * @ORM\Id()
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var Order[]|Collection
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="librarian")
	 */
	private $orders;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Returns the roles granted to the user.
	 * <code>
	 * public function getRoles()
	 * {
	 *     return array('ROLE_USER');
	 * }
	 * </code>
	 * Alternatively, the roles might be stored on a ``roles`` property,
	 * and populated in any number of different ways when the user object
	 * is created.
	 * @return Role[] The user roles
	 */
	public function getRoles()
	{
		return array_unique(array_merge(['ROLE_LIBRARIAN'], parent::getRoles()));
	}


	/**
	 * Add orders
	 *
	 * @param Order $orders
	 *
	 * @return Librarian
	 */
	public function addOrder(Order $orders)
	{
		$this->orders[] = $orders;

		return $this;
	}

	/**
	 * Remove orders
	 *
	 * @param Order $orders
	 */
	public function removeOrder(Order $orders)
	{
		$this->orders->removeElement($orders);
	}

	/**
	 * Get orders
	 * @return Collection
	 */
	public function getOrders()
	{
		return $this->orders;
	}
}
