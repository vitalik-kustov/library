<?php
/**
 * Created by PhpStorm.
 * Date: 10.07.2015
 * @author Vitaly
 */

namespace AppBundle\Entity\Listener;


use AppBundle\Entity\Order as OrderEntity;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;

class Order
{
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * Order constructor.
	 *
	 * @param LoggerInterface $logger
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	public function prePersist(OrderEntity $order, LifecycleEventArgs $eventArgs)
	{
		$this->logger->info('qwertynet');
	}

}