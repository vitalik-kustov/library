<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Order
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 * @ORM\EntityListeners({"AppBundle\Entity\Listener\Order"})
 * @package AppBundle\Entity
 */
class Order
{

	/**
	 * @var int
	 * @ORM\Id()
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @Assert\Valid()
	 * @Assert\Count(min="1")
	 * @var OrderItem[]|Collection
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderItem", mappedBy="order", cascade={"all"}, orphanRemoval=true)
	 */
	private $items;

	/**
	 * @Assert\NotBlank()
	 * @var Reader
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Reader", inversedBy="orders", fetch="EAGER")
	 * @ORM\JoinColumn(name="user_id", onDelete="CASCADE")
	 */
	private $reader;

	/**
	 * @Assert\NotBlank()
	 * @var Librarian
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Librarian", inversedBy="orders")
	 * @ORM\JoinColumn(name="librarian_id", onDelete="CASCADE")
	 */
	private $librarian;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="created_at", type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="updated_at", type="datetime")
	 * @Gedmo\Timestampable(on="update")
	 */
	private $updatedAt;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->items = new ArrayCollection();
	}

	/**
	 * Get id
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}


	/**
	 * Add items
	 *
	 * @param OrderItem $item
	 *
	 * @return Order
	 */
	public function addItem(OrderItem $item)
	{
		$item->setOrder($this);
		$this->items->add($item);

		return $this;
	}

	/**
	 * Remove items
	 *
	 * @param OrderItem $items
	 */
	public function removeItem(OrderItem $items)
	{
		$this->items->removeElement($items);
	}

	/**
	 * Get items
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * Set reader
	 *
	 * @param Reader $reader
	 *
	 * @return Order
	 */
	public function setReader(Reader $reader = null)
	{
		$this->reader = $reader;

		return $this;
	}

	/**
	 * Get reader
	 * @return Reader
	 */
	public function getReader()
	{
		return $this->reader;
	}

	/**
	 * Set librarian
	 *
	 * @param Librarian $librarian
	 *
	 * @return Order
	 */
	public function setLibrarian(Librarian $librarian = null)
	{
		$this->librarian = $librarian;

		return $this;
	}

	/**
	 * Get librarian
	 * @return Librarian
	 */
	public function getLibrarian()
	{
		return $this->librarian;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Order
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 *
	 * @return Order
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * Get updatedAt
	 * @return \DateTime
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}
}
