<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class OrderItem
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderItemRepository")
 * @ORM\Table(name="order_items")
 * @package AppBundle\Entity
 */
class OrderItem
{

	/**
	 * @var int
	 * @ORM\Id()
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @Assert\NotBlank()
	 * @var Order
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order", inversedBy="items", fetch="EAGER")
	 * @ORM\JoinColumn(name="order_id", nullable=false, onDelete="CASCADE")
	 */
	private $order;

	/**
	 * @Assert\NotBlank()
	 * @var Book
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", fetch="EAGER")
	 * @ORM\JoinColumn(name="book_id", onDelete="CASCADE", nullable=false)
	 */
	private $book;

	/**
	 * @Assert\Date()
	 * @var \DateTime
	 * @ORM\Column(name="expired_at", type="date")
	 */
	private $expiredAt;

	/**
	 * @var boolean
	 * @Assert\NotNull()
	 * @ORM\Column(name="closed", type="boolean", options={"default"="0"})
	 */
	private $closed;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="created_at", type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="updated_at", type="datetime")
	 * @Gedmo\Timestampable(on="update")
	 */
	private $updatedAt;


	/**
	 * Get id
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Set expiredAt
	 *
	 * @param \DateTime $expiredAt
	 *
	 * @return OrderItem
	 */
	public function setExpiredAt($expiredAt)
	{
		$this->expiredAt = $expiredAt;

		return $this;
	}

	/**
	 * Get expiredAt
	 * @return \DateTime
	 */
	public function getExpiredAt()
	{
		return $this->expiredAt;
	}

	/**
	 * Set order
	 *
	 * @param Order $order
	 *
	 * @return OrderItem
	 */
	public function setOrder(Order $order)
	{
		$this->order = $order;

		return $this;
	}

	/**
	 * Get order
	 * @return Order
	 */
	public function getOrder()
	{
		return $this->order;
	}

	/**
	 * Set book
	 *
	 * @param Book $book
	 *
	 * @return OrderItem
	 */
	public function setBook(Book $book)
	{
		$this->book = $book;

		return $this;
	}

	/**
	 * Get book
	 * @return Book
	 */
	public function getBook()
	{
		return $this->book;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return OrderItem
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 *
	 * @return OrderItem
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * Get updatedAt
	 * @return \DateTime
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}

	/**
	 * Set closed
	 *
	 * @param boolean $closed
	 *
	 * @return OrderItem
	 */
	public function setClosed($closed)
	{
		$this->closed = $closed;

		return $this;
	}

	/**
	 * Get closed
	 * @return boolean
	 */
	public function getClosed()
	{
		return $this->closed;
	}

	/**
	 * @return bool
	 */
	public function isExpired()
	{
		return (!$this->getClosed() && ($this->getExpiredAt() < new \DateTime('now')));
	}

}
