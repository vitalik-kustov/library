<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 03.06.2015
 * Time: 1:28
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAsserts;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class OrderItem
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderItemRepository")
 * @ORM\Table(name="order_items")
 * @package AppBundle\Entity
 */
class OrderItem {

	/**
	 * @var int
	 * @ORM\Id()
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @Assert\NotBlank()
	 * @var Order
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order", inversedBy="items", fetch="EAGER")
	 * @ORM\JoinColumn(name="order_id", nullable=false, onDelete="CASCADE")
	 */
	private $order;

	/**
	 * @Assert\NotBlank()
	 * @var Book
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", fetch="EAGER")
	 * @ORM\JoinColumn(name="book_id", onDelete="CASCADE", nullable=false)
	 */
	private $book;

	/**
	 * @AppAsserts\InFuture()
	 * @Assert\Date()
	 * @var \DateTime
	 * @ORM\Column(name="expired_at", type="date")
	 */
	private $expiredAt;

	/**
	 * @var boolean
	 * @Assert\NotNull()
	 * @ORM\Column(name="closed", type="boolean", options={"default"="0"})
	 */
	private $closed;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="created_at", type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="updated_at", type="datetime")
	 * @Gedmo\Timestampable(on="update")
	 */
	private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

    /**
     * Set expiredAt
     *
     * @param \DateTime $expiredAt
     * @return OrderItem
     */
    public function setExpiredAt($expiredAt)
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    /**
     * Get expiredAt
     *
     * @return \DateTime 
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set order
     *
     * @param \AppBundle\Entity\Order $order
     * @return OrderItem
     */
    public function setOrder(\AppBundle\Entity\Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AppBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set book
     *
     * @param \AppBundle\Entity\Book $book
     * @return OrderItem
     */
    public function setBook(\AppBundle\Entity\Book $book)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return \AppBundle\Entity\Book 
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return OrderItem
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return OrderItem
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
