<?php

namespace AppBundle\Entity;

/**
 * Class OrderItemsCollection
 * @package AppBundle\Entity
 */
class OrderItemsCollection
{

	public $orderItems;

	public $limit;

	public $offset;

	public function __construct($orderItems, $limit, $offset)
	{
		$this->orderItems = $orderItems;
		$this->limit = $limit;
		$this->offset = $offset;
	}
}