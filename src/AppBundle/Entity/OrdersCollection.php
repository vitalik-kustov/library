<?php

namespace AppBundle\Entity;

/**
 * Class OrdersCollection
 * @package AppBundle\Entity
 */
class OrdersCollection {

	public $orders;

	public $limit;

	public $offset;

	public function __construct($orders, $limit, $offset)
	{
		$this->orders = $orders;
		$this->limit = $limit;
		$this->offset = $offset;
	}
}