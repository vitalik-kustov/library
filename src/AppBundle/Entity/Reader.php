<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;

/**
 * Class User
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 * @ORM\Table(name="users")
 * @package AppBundle\Entity
 */
class Reader extends BaseUser
{

	/**
	 * @var int
	 * @ORM\Id()
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var Order[]|Collection
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="reader")
	 */
	private $orders;

	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * Get id
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Add orders
	 *
	 * @param Order $orders
	 *
	 * @return Reader
	 */
	public function addOrder(Order $orders)
	{
		$this->orders[] = $orders;

		return $this;
	}

	/**
	 * Remove orders
	 *
	 * @param Order $orders
	 */
	public function removeOrder(Order $orders)
	{
		$this->orders->removeElement($orders);
	}

	/**
	 * Get orders
	 * @return Collection
	 */
	public function getOrders()
	{
		return $this->orders;
	}

	public function getRoles()
	{
		return array_unique(array_merge(['ROLE_READER'], parent::getRoles()));
	}


}
