<?php

namespace AppBundle\Entity;

/**
 * Class ReadersCollection
 * @package AppBundle\Entity
 */
class ReadersCollection
{

	public $readers;

	public $limit;

	public $offset;

	/**
	 * @param $readers
	 * @param $limit
	 * @param $offset
	 */
	public function __construct($readers, $limit, $offset)
	{
		$this->readers = $readers;
		$this->limit = $limit;
		$this->offset = $offset;
	}

}