<?php

namespace AppBundle\Exception;

/**
 * Class InvalidFormException
 * @package AppBundle\Exception
 */
class InvalidFormException extends \RuntimeException
{
	private $form;
	private $prev;

	public function __construct($message, $form = null, \Exception $prev = null)
	{
		parent::__construct($message, -1, $prev);
		$this->form = $form;
		$this->prev = $prev;
	}

	/**
	 * @return array|null
	 * @throws \Exception
	 */
	public function getForm()
	{
		if (!$this->form) {
			throw $this->prev;
		}

		return $this->form;
	}
}