<?php

namespace AppBundle\Form\Model;


use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class Order
 * @package AppBundle\Form\Model
 */
class Order
{

	public $id;

	/**
	 * @var int
	 * @NotNull()
	 */
	public $librarianId;

}