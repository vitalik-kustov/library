<?php

namespace AppBundle\Form\Model;


use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class OrderItem
 * @package AppBundle\Form\Model
 */
class OrderItem
{

	public $id;

	/**
	 * @var int
	 * @NotNull()
	 */
	public $bookId;

	/**
	 * @var \DateTime
	 * @Date()
	 * @NotNull()
	 */
	public $expiredAt;

	/**
	 * @var boolean
	 * @NotNull()
	 */
	public $closed;

}