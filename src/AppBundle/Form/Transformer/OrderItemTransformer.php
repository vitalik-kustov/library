<?php

namespace AppBundle\Form\Transformer;


use AppBundle\Entity\OrderItem as Item;
use AppBundle\Form\Model\OrderItem;
use AppBundle\Repository\BookRepositoryInterface;
use AppBundle\Repository\OrderItemRepositoryInterface;

/**
 * Class OrderItemTransformer
 * @package AppBundle\Form\Transformer
 */
class OrderItemTransformer implements Transformer
{

	/**
	 * @var OrderItemRepositoryInterface
	 */
	private $orderItemRepository;

	/**
	 * @var BookRepositoryInterface
	 */
	private $bookRepository;

	/**
	 * @param OrderItemRepositoryInterface $orderItemRepository
	 * @param BookRepositoryInterface $bookRepository
	 */
	public function __construct(
		OrderItemRepositoryInterface $orderItemRepository,
		BookRepositoryInterface $bookRepository
	)
	{
		$this->orderItemRepository = $orderItemRepository;
		$this->bookRepository = $bookRepository;
	}


	/**
	 * @param OrderItem $dataTransferObject
	 *
	 * @return Item entity object
	 * @throws \Exception
	 */
	public function transformToEntity($dataTransferObject)
	{
		/** @var OrderItem $dataTransferObject */
		if ($dataTransferObject->id) {
			$entity = $this->orderItemRepository->find($dataTransferObject->id);
			if (!$entity) {
				throw new \Exception('Invalid id parameter');
			}
		} else {
			$entity = new Item();
		}

		$book = $this->bookRepository->find($dataTransferObject->bookId);
		if (!$book) {
			throw new \Exception('Invalid bookId parameter');
		}

		$entity->setBook($book);
		$entity->setClosed($dataTransferObject->closed);
		$entity->setExpiredAt(new \DateTime($dataTransferObject->expiredAt));

		return $entity;
	}

	/**
	 * @param Item $entityObject
	 *
	 * @return OrderItem data transfer object
	 */
	public function transformToDTO($entityObject)
	{
		/** @var Item $entityObject */
		$dto = new OrderItem();
		if ($entityObject->getId()) {
			$dto->id = $entityObject->getId();
			$dto->expiredAt = $entityObject->getExpiredAt()
				->format('Y-m-d');
			$dto->closed = $entityObject->getClosed();
			if (false !== ($book = $entityObject->getBook())) {
				$dto->bookId = $book->getId();
			}
		}

		return $dto;
	}
}