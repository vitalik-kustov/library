<?php

namespace AppBundle\Form\Transformer;


use AppBundle\Entity\Order as OrderEntity;
use AppBundle\Form\Model\Order;
use AppBundle\Repository\LibrarianRepositoryInterface;
use AppBundle\Repository\OrderRepositoryInterface;

/**
 * Class OrderTransformer
 * @package AppBundle\Form\Transformer
 */
class OrderTransformer implements Transformer
{

	/**
	 * @var OrderRepositoryInterface
	 */
	private $orderRepository;

	/**
	 * @var LibrarianRepositoryInterface
	 */
	private $librarianRepository;

	/**
	 * @param OrderRepositoryInterface $orderRepository
	 * @param LibrarianRepositoryInterface $librarianRepository
	 */
	public function __construct(
		OrderRepositoryInterface $orderRepository,
		LibrarianRepositoryInterface $librarianRepository
	)
	{
		$this->orderRepository = $orderRepository;
		$this->librarianRepository = $librarianRepository;
	}


	/**
	 * @param $dataTransferObject
	 *
	 * @return object entity object
	 * @throws \Exception
	 */
	public function transformToEntity($dataTransferObject)
	{
		/** @var Order $dataTransferObject */
		if ($dataTransferObject->id) {
			$order = $this->orderRepository->find($dataTransferObject->id);
			if (!$order) {
				throw new \Exception('Incorrect id parameter');
			}
		} else {
			$order = new OrderEntity();
		}


		$librarian = $this->librarianRepository->find($dataTransferObject->librarianId);
		if (!$librarian) {
			throw new \Exception('Incorrect librarian parameter');
		}

		$order
			->setLibrarian($librarian);

		return $order;
	}

	/**
	 * @param $entityObject
	 *
	 * @return object data transfer object
	 */
	public function transformToDTO($entityObject)
	{
		/** @var OrderEntity $entityObject */
		$model = new Order();
		if ($entityObject->getId()) {
			$model->id = $entityObject->getId();

			if ($entityObject->getLibrarian()) {
				$model->librarianId = $entityObject->getLibrarian()
					->getId();
			}
		}

		return $model;
	}

}