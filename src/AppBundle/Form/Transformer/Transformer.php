<?php

namespace AppBundle\Form\Transformer;

/**
 * Interface Transformer
 * @package AppBundle\Form\Transformer
 */
interface Transformer {

	/**
	 * @param $dataTransferObject
	 *
	 * @return object entity object
	 */
	public function transformToEntity($dataTransferObject);

	/**
	 * @param $entityObject
	 *
	 * @return object data transfer object
	 */
	public function transformToDTO($entityObject);

}