<?php

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class OrderFormType
 * @package AppBundle\Form\Type
 */
class OrderFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('items', 'collection', [
			'type' => new OrderItemFormType(),
			'options' => [
				'required' => true,
			],
			'required' => true,
			'by_reference' => false,
			'label' => 'Order Items',
			'allow_add' => true,
			'allow_delete' => true,
		]);
		$builder->add('reader', 'entity', [
			'class' => 'AppBundle\Entity\Reader',
			'property' => 'username',
			'label' => 'Reader',
			'required' => true,
		]);

		$librarianOptions = [
			'class' => 'AppBundle\Entity\Librarian',
			'property' => 'username',
			'label' => 'Librarian',
		];
		if ($options['data']->getId() > 0) {
			$librarianOptions['disabled'] = true;
		}

		$builder->add('librarian', 'entity', $librarianOptions);
		$builder->add('id', 'hidden');
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\Order'
		]);
	}


	/**
	 * Returns the name of this type.
	 * @return string The name of this type
	 */
	public function getName()
	{
		return 'app_order';
	}
}