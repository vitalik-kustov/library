<?php

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class OrderItemFormType
 * @package AppBundle\Form\Type
 */
class OrderItemFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('expiredAt', 'date', [
			'label' => 'Expired at',
			'required' => true,
		]);
		$builder->add('book', 'entity', [
			'class' => 'AppBundle:Book',
			'property' => 'name',
			'required' => true,
			'label' => 'Book'
		]);
		$builder->add('closed', 'checkbox', [
			'label' => 'Closed ?',
			'required' => false,
		]);
		$builder->add('id', 'hidden');
		$builder->add('btn', 'button', [
			'label' => 'Kill',
			'attr' => [
				'class' => 'kill-btn'
			],
		]);
	}


	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\OrderItem',
			'csrf_protection' => false,
		]);
	}


	/**
	 * Returns the name of this type.
	 * @return string The name of this type
	 */
	public function getName()
	{
		return 'app_order_item';
	}
}