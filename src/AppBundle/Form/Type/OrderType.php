<?php

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class OrderType
 * @package AppBundle\Form\Type
 */
class OrderType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('id', 'integer');
		$builder->add('librarianId', 'integer');
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Form\Model\Order'
		]);
	}


	/**
	 * Returns the name of this type.
	 * @return string The name of this type
	 */
	public function getName()
	{
		return 'order_form_type';
	}
}