<?php

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ReaderType
 * @package AppBundle\Form\Type
 */
class ReaderType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('username', 'text')
			->add('email', 'email')
			->add('password', 'password');
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\Reader',
		]);
	}


	/**
	 * Returns the name of this type.
	 * @return string The name of this type
	 */
	public function getName()
	{
		return 'reader_form_type';
	}
}