<?php

namespace AppBundle\Handler;


use AppBundle\Entity\DeptorsCollection;
use AppBundle\Repository\ReaderRepositoryInterface;

class DeptorHandler implements DeptorHandlerInterface
{

	/**
	 * @var ReaderRepositoryInterface
	 */
	private $readerRepository;

	public function __construct(ReaderRepositoryInterface $readerRepository)
	{
		$this->readerRepository = $readerRepository;
	}


	/**
	 * @param $limit
	 * @param $offset
	 *
	 * @return DeptorsCollection
	 */
	public function all($limit, $offset)
	{
		$deptors = $this->readerRepository->findDeptorsWithDebtCount()
			->setMaxResults($limit)
			->setFirstResult($offset)
			->getResult();

		return new DeptorsCollection(
			$deptors,
			$limit,
			$offset
		);
	}
}