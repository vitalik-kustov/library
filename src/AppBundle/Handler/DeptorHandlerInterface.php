<?php

namespace AppBundle\Handler;
use AppBundle\Entity\DeptorsCollection;

/**
 * Interface DeptorHandlerInterface
 * @package AppBundle\Handler
 */
interface DeptorHandlerInterface {

	/**
	 * @param $limit
	 * @param $offset
	 *
	 * @return DeptorsCollection
	 */
	public function all($limit, $offset);

}