<?php

namespace AppBundle\Handler;


use AppBundle\Entity\Order;
use AppBundle\Entity\OrdersCollection;
use AppBundle\Exception\InvalidFormException;
use AppBundle\Form\Model\Order as ModelOrder;
use AppBundle\Form\Transformer\OrderTransformer;
use AppBundle\Form\Type\OrderType;
use AppBundle\Repository\LibrarianRepository;
use AppBundle\Repository\OrderRepositoryInterface;
use AppBundle\Repository\ReaderRepositoryInterface;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class OrderHandler
 * @package AppBundle\Handler
 */
class OrderHandler implements OrderHandlerInterface
{
	/**
	 * @var OrderRepositoryInterface
	 */
	private $orderRepository;

	/**
	 * @var FormFactoryInterface
	 */
	private $formFactory;

	/**
	 * @var ReaderRepositoryInterface
	 */
	private $readerRepository;

	/**
	 * @var OrderTransformer
	 */
	private $orderTransformer;

	/**
	 * @param OrderRepositoryInterface $orderRepository
	 * @param FormFactoryInterface $formFactory
	 * @param ReaderRepositoryInterface $readerRepository
	 * @param OrderTransformer $orderTransformer
	 */
	public function __construct(
		OrderRepositoryInterface $orderRepository,
		FormFactoryInterface $formFactory,
		ReaderRepositoryInterface $readerRepository,
		OrderTransformer $orderTransformer
	)
	{
		$this->orderRepository = $orderRepository;
		$this->formFactory = $formFactory;
		$this->readerRepository = $readerRepository;
		$this->orderTransformer = $orderTransformer;
	}


	/**
	 * Get the reader's orders collection
	 *
	 * @param $readerId
	 * @param int $limit
	 * @param int $offset
	 * @param string $sortBy
	 * @param string $sortType
	 *
	 * @return mixed
	 */
	public function all($readerId, $limit = 5, $offset = 0, $sortBy = 'createdAt', $sortType = 'ASC')
	{
		$orders = $this->orderRepository->findBy([
			'reader' => $readerId
		], [
			$sortBy => $sortType
		], $limit, $offset);

		return new OrdersCollection($orders, $limit, $offset);
	}

	/**
	 * Get the reader's single order
	 *
	 * @param $readerId
	 * @param $id
	 *
	 * @return mixed
	 */
	public function get($readerId, $id)
	{
		return $this->orderRepository->findOneBy([
			'id' => $id,
			'reader' => $readerId
		]);
	}

	/**
	 * Post the order
	 *
	 * @param $readerId
	 * @param array $parameters
	 *
	 * @return mixed
	 */
	public function post($readerId, array $parameters)
	{
		$model = new Order();

		return $this->process($readerId, $model, $parameters, 'POST');
	}

	/**
	 * Put the order
	 *
	 * @param Order $order
	 * @param $readerId
	 * @param array $parameters
	 *
	 * @return mixed
	 */
	public function put(Order $order, $readerId, array $parameters)
	{
		return $this->process($readerId, $order, $parameters, 'PUT');
	}

	/**
	 * Patch the order
	 *
	 * @param Order $order
	 * @param $readerId
	 * @param array $parameters
	 *
	 * @return mixed
	 */
	public function patch(Order $order, $readerId, array $parameters)
	{
		return $this->process($readerId, $order, $parameters, 'PATCH');
	}

	/**
	 * Delete the order
	 *
	 * @param Order $order
	 * @param $readerId
	 *
	 * @return void
	 */
	public function delete(Order $order, $readerId)
	{
		$toDeleteOrder = $this->get($readerId, $order->getId());
		if ($toDeleteOrder && $toDeleteOrder->getId() === $order->getId()) {
			$this->orderRepository->deleteOrder($order);
		}
	}

	private function process($readerId, Order $order, array $parameters, $method = 'PUT')
	{
		$form = $this->formFactory->create(new OrderType(), $this->orderTransformer->transformToDTO($order),
			['method' => $method]);
		$form->submit($parameters, $method !== 'PATCH');
		if ($form->isValid()) {
			$order = $this->orderTransformer->transformToEntity($form->getData());
			$reader = $this->readerRepository->find($readerId);
			if (!$reader) {
				throw new \Exception('Incorrect reader parameter');
			}
			$order->setReader($reader);
			$this->orderRepository->saveOrder($order);

			return $order;
		}

		throw new InvalidFormException('Invalid submitted data', $form);
	}

}