<?php

namespace AppBundle\Handler;


use AppBundle\Entity\Order;

/**
 * Interface OrderHandlerInterface
 * @package AppBundle\Handler
 */
interface OrderHandlerInterface
{

	/**
	 * Get the reader's orders collection
	 *
	 * @param $readerId
	 * @param int $limit
	 * @param int $offset
	 * @param string $sortBy
	 * @param string $sortType
	 *
	 * @return mixed
	 */
	public function all($readerId, $limit = 5, $offset = 0, $sortBy = 'createdAt', $sortType = 'ASC');

	/**
	 * Get the reader's single order
	 *
	 * @param $readerId
	 * @param $id
	 *
	 * @return mixed
	 */
	public function get($readerId, $id);

	/**
	 * Post the order
	 *
	 * @param $readerId
	 * @param array $parameters
	 *
	 * @return mixed
	 */
	public function post($readerId, array $parameters);

	/**
	 * Put the order
	 *
	 * @param Order $order
	 * @param $readerId
	 * @param array $parameters
	 *
	 * @return mixed
	 */
	public function put(Order $order, $readerId, array $parameters);

	/**
	 * Patch the order
	 *
	 * @param Order $order
	 * @param $readerId
	 * @param array $parameters
	 *
	 * @return mixed
	 */
	public function patch(Order $order, $readerId, array $parameters);

	/**
	 * Delete the order
	 *
	 * @param Order $order
	 * @param $readerId
	 *
	 * @return void
	 */
	public function delete(Order $order, $readerId);

}