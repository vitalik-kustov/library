<?php

namespace AppBundle\Handler;


use AppBundle\Entity\OrderItem;
use AppBundle\Entity\OrderItemsCollection;
use AppBundle\Exception\InvalidFormException;
use AppBundle\Form\Transformer\OrderItemTransformer;
use AppBundle\Form\Type\OrderItemType;
use AppBundle\Repository\OrderItemRepositoryInterface;
use AppBundle\Repository\OrderRepositoryInterface;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class OrderItemHandler
 * @package AppBundle\Handler
 */
class OrderItemHandler implements OrderItemHandlerInterface
{
	/**
	 * @var OrderItemRepositoryInterface
	 */
	private $orderItemRepository;

	/**
	 * @var FormFactoryInterface
	 */
	private $formFactory;

	/**
	 * @var OrderItemTransformer
	 */
	private $transformer;

	/**
	 * @var OrderRepositoryInterface
	 */
	private $orderRepository;

	/**
	 * @param OrderItemRepositoryInterface $orderItemRepository
	 * @param FormFactoryInterface $formFactory
	 * @param OrderItemTransformer $transformer
	 * @param OrderRepositoryInterface $orderRepository
	 */
	public function __construct(
		OrderItemRepositoryInterface $orderItemRepository,
		FormFactoryInterface $formFactory,
		OrderItemTransformer $transformer,
		OrderRepositoryInterface $orderRepository
	)
	{
		$this->orderItemRepository = $orderItemRepository;
		$this->formFactory = $formFactory;
		$this->transformer = $transformer;
		$this->orderRepository = $orderRepository;
	}


	/**
	 * Get order items
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param int $limit
	 * @param int $offset
	 * @param string $sortBy
	 * @param string $sortType
	 *
	 * @return OrderItemsCollection
	 */
	public function all($readerId, $orderId, $limit = 5, $offset = 0, $sortBy = 'id', $sortType = 'ASC')
	{
		$items = $this->orderItemRepository->findOrderItems($orderId, $readerId, $sortBy, $sortType, $limit, $offset);

		return new OrderItemsCollection(
			$items,
			$limit,
			$offset
		);
	}

	/**
	 * Get single order items
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param $id
	 *
	 * @return OrderItem|null
	 */
	public function get($readerId, $orderId, $id)
	{
		return $this->orderItemRepository->findOrderItem($id, $orderId, $readerId);
	}

	/**
	 * Post order item
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param array $parameters
	 *
	 * @return OrderItem
	 */
	public function post($readerId, $orderId, array $parameters)
	{
		$item = new OrderItem();

		return $this->process($item, $readerId, $orderId, $parameters, 'POST');
	}

	/**
	 * Put order item
	 *
	 * @param OrderItem $orderItem
	 * @param $readerId
	 * @param $orderId
	 * @param array $parameters
	 *
	 * @return OrderItem
	 */
	public function put(OrderItem $orderItem, $readerId, $orderId, array $parameters)
	{
		return $this->process($orderItem, $readerId, $orderId, $parameters, 'PUT');
	}

	/**
	 * Patch order item
	 *
	 * @param OrderItem $orderItem
	 * @param $readerId
	 * @param $orderId
	 * @param array $parameters
	 *
	 * @return OrderItem
	 */
	public function patch(OrderItem $orderItem, $readerId, $orderId, array $parameters)
	{
		return $this->process($orderItem, $readerId, $orderId, $parameters, 'PATCH');
	}

	/**
	 * Delete order item
	 *
	 * @param OrderItem $orderItem
	 *
	 * @return void
	 */
	public function delete(OrderItem $orderItem)
	{
		$this->orderItemRepository->deleteOrderItem($orderItem);
	}

	/**
	 * @param OrderItem $item
	 * @param $readerId
	 * @param $orderId
	 * @param array $parameters
	 * @param $method
	 *
	 * @return OrderItem|object
	 * @throws \Exception
	 */
	private function process(OrderItem $item, $readerId, $orderId, array $parameters, $method)
	{
		$form = $this->formFactory->create(
			new OrderItemType(),
			$this->transformer->transformToDTO($item),
			['method' => $method]
		);
		$form->submit($parameters, $method !== 'PATCH');
		if ($form->isValid()) {
			$item = $this->transformer->transformToEntity($form->getData());

			$order = $this->orderRepository->findOneBy([
				'id' => $orderId,
				'reader' => $readerId
			]);

			if (!$order) {
				throw new \Exception('Cannot process order item');
			}
			$item->setOrder($order);
			$this->orderItemRepository->saveOrderItem($item);

			return $item;
		}

		throw new InvalidFormException('Invalid submitted form data', $form);
	}
}