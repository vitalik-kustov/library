<?php

namespace AppBundle\Handler;


use AppBundle\Entity\OrderItem;
use AppBundle\Entity\OrderItemsCollection;

/**
 * Interface OrderItemHandlerInterface
 * @package AppBundle\Handler
 */
interface OrderItemHandlerInterface
{
	/**
	 * Get order items
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param int $limit
	 * @param int $offset
	 * @param string $sortBy
	 * @param string $sortType
	 *
	 * @return OrderItemsCollection
	 */
	public function all($readerId, $orderId, $limit = 5, $offset = 0, $sortBy = 'id', $sortType = 'ASC');

	/**
	 * Get single order items
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param $id
	 *
	 * @return OrderItem
	 */
	public function get($readerId, $orderId, $id);

	/**
	 * Post order item
	 *
	 * @param $readerId
	 * @param $orderId
	 * @param array $parameters
	 *
	 * @return OrderItem
	 */
	public function post($readerId, $orderId, array $parameters);

	/**
	 * Put order item
	 *
	 * @param OrderItem $orderItem
	 * @param $readerId
	 * @param $orderId
	 * @param array $parameters
	 *
	 * @return OrderItem
	 */
	public function put(OrderItem $orderItem, $readerId, $orderId, array $parameters);

	/**
	 * Patch order item
	 *
	 * @param OrderItem $orderItem
	 * @param $readerId
	 * @param $orderId
	 * @param array $parameters
	 *
	 * @return OrderItem
	 */
	public function patch(OrderItem $orderItem, $readerId, $orderId, array $parameters);

	/**
	 * Delete order item
	 *
	 * @param OrderItem $orderItem
	 *
	 * @return void
	 */
	public function delete(OrderItem $orderItem);

}