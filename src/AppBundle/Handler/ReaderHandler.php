<?php

namespace AppBundle\Handler;


use AppBundle\Entity\Reader;
use AppBundle\Entity\ReadersCollection;
use AppBundle\Exception\InvalidFormException;
use AppBundle\Form\Type\ReaderType;
use AppBundle\Repository\ReaderRepositoryInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class ReaderHandler
 * @package AppBundle\Handler
 */
class ReaderHandler implements ReaderHandlerInterface
{
	/**
	 * @var UserManagerInterface
	 */
	private $userManager;

	/**
	 * @var FormFactoryInterface
	 */
	private $formFactory;

	/**
	 * @var ReaderRepositoryInterface
	 */
	private $readersRepository;

	/**
	 * @param UserManagerInterface $userManager
	 * @param FormFactoryInterface $formFactory
	 * @param ReaderRepositoryInterface $readersRepository
	 */
	public function __construct(
		UserManagerInterface $userManager,
		FormFactoryInterface $formFactory,
		ReaderRepositoryInterface $readersRepository
	)
	{
		$this->userManager = $userManager;
		$this->formFactory = $formFactory;
		$this->readersRepository = $readersRepository;
	}


	/**
	 * Get a list of Readers.
	 *
	 * @param int $limit the limit of the result
	 * @param int $offset starting from the offset
	 * @param string $sortBy
	 * @param string $sortType
	 *
	 * @return ReadersCollection
	 */
	public function all($limit = 5, $offset = 0, $sortBy = 'id', $sortType = 'ASC')
	{
		$readers = $this->readersRepository->findBy([], [$sortBy => $sortType], $limit, $offset);
		return new ReadersCollection(
			$readers,
			$limit,
			$offset
		);
	}

	/**
	 * Post Reader, creates a new Reader.
	 * @api
	 *
	 * @param array $parameters
	 *
	 * @return Reader
	 */
	public function post(array $parameters)
	{
		$reader = $this->userManager->createUser();

		return $this->process($reader, $parameters, 'POST');
	}

	/**
	 * Edit a Reader.
	 * @api
	 *
	 * @param Reader $reader
	 * @param array $parameters
	 *
	 * @return Reader
	 */
	public function put(Reader $reader, array $parameters)
	{
		return $this->process($reader, $parameters, 'PUT');
	}

	/**
	 * Partially update a Reader.
	 * @api
	 *
	 * @param Reader $reader
	 * @param array $parameters
	 *
	 * @return Reader
	 */
	public function patch(Reader $reader, array $parameters)
	{
		return $this->process($reader, $parameters, 'PATCH');
	}

	/**
	 * Process the form
	 *
	 * @param Reader $reader
	 * @param array $parameters
	 * @param string $method
	 *
	 * @return Reader|mixed
	 * @throws InvalidFormException
	 */
	private function process(Reader $reader, array $parameters, $method = 'PUT')
	{
		$form = $this->formFactory->create(new ReaderType(), $reader, ['method' => $method]);
		$form->submit($parameters, $method !== 'PATCH');
		if ($form->isValid()) {
			/** @var Reader $reader */
			$reader = $form->getData();
			$reader->setPlainPassword($reader->getPassword());
			$reader->setEnabled(true);
			$this->userManager->updatePassword($reader);
			$this->userManager->updateUser($reader);

			return $reader;
		}

		throw new InvalidFormException('Invalid submitted data', $form);
	}

	/**
	 * Get a Reader given the identifier
	 * @api
	 *
	 * @param mixed $id
	 *
	 * @return Reader
	 */
	public function getSingle($id)
	{
		return $this->readersRepository->find($id);
	}

	/**
	 * Delete reader
	 * @api
	 *
	 * @param Reader $reader
	 *
	 * @return void
	 */
	public function delete(Reader $reader)
	{
		$this->userManager->deleteUser($reader);
	}
}