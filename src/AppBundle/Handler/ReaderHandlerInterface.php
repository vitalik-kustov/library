<?php

namespace AppBundle\Handler;


use AppBundle\Entity\Reader;
use AppBundle\Entity\ReadersCollection;

interface ReaderHandlerInterface
{

	/**
	 * Get a Reader given the identifier
	 * @api
	 *
	 * @param mixed $id
	 *
	 * @return Reader
	 */
	public function getSingle($id);

	/**
	 * Get a list of Readers.
	 *
	 * @param int $limit the limit of the result
	 * @param int $offset starting from the offset
	 * @param string $sortBy
	 * @param string $sortType
	 *
	 * @return ReadersCollection
	 */
	public function all($limit = 5, $offset = 0, $sortBy = 'id', $sortType = 'ASC');

	/**
	 * Post Reader, creates a new Reader.
	 * @api
	 *
	 * @param array $parameters
	 *
	 * @return Reader
	 */
	public function post(array $parameters);

	/**
	 * Edit a Reader.
	 * @api
	 *
	 * @param Reader $reader
	 * @param array $parameters
	 *
	 * @return Reader
	 */
	public function put(Reader $reader, array $parameters);

	/**
	 * Partially update a Reader.
	 * @api
	 *
	 * @param Reader $reader
	 * @param array $parameters
	 *
	 * @return Reader
	 */
	public function patch(Reader $reader, array $parameters);

	/**
	 * Delete reader
	 * @api
	 *
	 * @param Reader $reader
	 *
	 * @return void
	 */
	public function delete(Reader $reader);
}