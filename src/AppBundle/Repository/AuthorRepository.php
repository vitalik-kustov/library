<?php

namespace AppBundle\Repository;

use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Class AuthorRepository
 * @package AppBundle\Repository
 */
class AuthorRepository extends EntityRepository implements AuthorRepositoryInterface
{
	/**
	 * @param \DateTime $startDate
	 * @param \DateTime $finishDate
	 *
	 * @return mixed
	 */
	public function findTop10Authors(\DateTime $startDate, \DateTime $finishDate)
	{
		$authors = $this->_em->createQuery(
			'SELECT authors as author, COUNT(item.id) as itemsCount
			FROM AppBundle:Author authors
			JOIN authors.books books
			JOIN AppBundle:OrderItem item WITH (item.book = books.id AND item.createdAt BETWEEN :startDate AND :finishDate)
			GROUP BY authors.id
			ORDER BY itemsCount DESC'
		)
			->setMaxResults(10)
			->setParameter('startDate', $startDate, DateType::DATE)
			->setParameter('finishDate', $finishDate, DateType::DATE)
			->getResult();

		return $authors;
	}
}
