<?php

namespace AppBundle\Repository;


use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Interface AuthorRepositoryInterface
 * @package AppBundle\Repository
 */
interface AuthorRepositoryInterface extends ObjectRepository
{
	/**
	 * @param \DateTime $startDate
	 * @param \DateTime $finishDate
	 *
	 * @return mixed
	 */
	public function findTop10Authors(\DateTime $startDate, \DateTime $finishDate);
}