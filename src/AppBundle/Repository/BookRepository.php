<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class BookRepository
 * @package AppBundle\Repository
 */
class BookRepository extends EntityRepository implements BookRepositoryInterface
{
}
