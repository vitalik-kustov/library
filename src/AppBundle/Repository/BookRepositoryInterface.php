<?php

namespace AppBundle\Repository;


use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Interface BookRepositoryInterface
 * @package AppBundle\Repository
 */
interface BookRepositoryInterface extends ObjectRepository
{

}