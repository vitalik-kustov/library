<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class LibrarianRepository
 * @package AppBundle\Repository
 */
class LibrarianRepository extends EntityRepository implements LibrarianRepositoryInterface
{
}
