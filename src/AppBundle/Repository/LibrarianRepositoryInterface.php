<?php

namespace AppBundle\Repository;


use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Interface LibrarianRepositoryInterface
 * @package AppBundle\Repository
 */
interface LibrarianRepositoryInterface extends ObjectRepository
{

}