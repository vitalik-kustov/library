<?php

namespace AppBundle\Repository;

use AppBundle\Entity\OrderItem;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityRepository;

/**
 * Class OrderItemRepository
 * @package AppBundle\Repository
 */
class OrderItemRepository extends EntityRepository implements OrderItemRepositoryInterface
{
	/**
	 * Find single order item
	 *
	 * @param $id
	 * @param $orderId
	 * @param $readerId
	 *
	 * @return OrderItem|null
	 */
	public function findOrderItem($id, $orderId, $readerId)
	{
		$query = $this->_em->createQuery(
			'SELECT
				item
			FROM AppBundle:OrderItem item
				INNER JOIN item.order _order WITH _order.id = :orderId
				INNER JOIN _order.reader reader WITH reader.id = :readerId
			WHERE item.id = :id'
		)
			->setParameter('id', $id, IntegerType::INTEGER)
			->setParameter('orderId', $orderId, IntegerType::INTEGER)
			->setParameter('readerId', $readerId, IntegerType::INTEGER);

		return $query->getSingleResult();
	}

	/**
	 * Find order items collection
	 *
	 * @param $orderId
	 * @param $readerId
	 * @param $sortBy
	 * @param $sortType
	 * @param $limit
	 * @param $offset
	 *
	 * @return \AppBundle\Entity\OrderItem[]|Collection
	 */
	public function findOrderItems($orderId, $readerId, $sortBy, $sortType, $limit, $offset)
	{
		$query = $this->_em->createQuery(
			'SELECT
				item, o, reader, b, a
			FROM AppBundle:OrderItem item
				INNER JOIN item.order o WITH o.id = :orderId
				INNER JOIN o.reader reader WITH reader.id = :readerId
				INNER JOIN item.book b
				INNER JOIN b.authors a
			ORDER BY item.' . $sortBy . ' ' . $sortType
		)
			->setMaxResults($limit)
			->setFirstResult($offset)
			->setParameter('orderId', $orderId, IntegerType::INTEGER)
			->setParameter('readerId', $readerId, IntegerType::INTEGER);

		return $query->getResult();
	}

	/**
	 * @param OrderItem $item
	 *
	 * @return void
	 */
	public function saveOrderItem(OrderItem $item)
	{
		$this->_em->persist($item);
		$this->_em->flush();
	}

	/**
	 * @param OrderItem $orderItem
	 *
	 * @return void
	 */
	public function deleteOrderItem(OrderItem $orderItem)
	{
		$this->_em->remove($orderItem);
		$this->_em->flush();
	}
}
