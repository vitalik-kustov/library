<?php

namespace AppBundle\Repository;


use AppBundle\Entity\OrderItem;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Interface OrderItemRepositoryInterface
 * @package AppBundle\Repository
 */
interface OrderItemRepositoryInterface extends ObjectRepository {

	/**
	 * Find single order item
	 *
	 * @param $id
	 * @param $orderId
	 * @param $readerId
	 *
	 * @return OrderItem|null
	 */
	public function findOrderItem($id, $orderId, $readerId);

	/**
	 * Find order items collection
	 *
	 * @param $orderId
	 * @param $readerId
	 * @param $sortBy
	 * @param $sortType
	 * @param $limit
	 * @param $offset
	 *
	 * @return \AppBundle\Entity\OrderItem[]|Collection
	 */
	public function findOrderItems($orderId, $readerId, $sortBy, $sortType, $limit, $offset);

	/**
	 * @param OrderItem $item
	 *
	 * @return void
	 */
	public function saveOrderItem(OrderItem $item);

	/**
	 * @param OrderItem $orderItem
	 *
	 * @return void
	 */
	public function deleteOrderItem(OrderItem $orderItem);

}