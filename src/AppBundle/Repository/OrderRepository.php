<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Order;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class OrderRepository
 * @package AppBundle\Repository
 */
class OrderRepository extends EntityRepository implements OrderRepositoryInterface
{
	/**
	 * @var TokenStorageInterface
	 */
	private $tokenStorage;

	/**
	 * @var boolean
	 */
	private $showEmptyOrders;

	/**
	 * @param TokenStorageInterface $tokenStorage
	 */
	public function setTokenStorage(TokenStorageInterface $tokenStorage)
	{
		$this->tokenStorage = $tokenStorage;
	}

	/**
	 * @param $showEmptyOrders
	 */
	public function setShowEmptyOrders($showEmptyOrders)
	{
		$this->showEmptyOrders = $showEmptyOrders;
	}

	/**
	 * @param $owner
	 * @param $state
	 * @param $expired
	 *
	 * @return Query
	 */
	public function findLibrarianFilteredOrders($owner, $state, $expired)
	{
		$qb = $this->createQueryBuilder('o');
		if ($this->showEmptyOrders) {
			$qb->leftJoin('o.items', 'i');
		} else {
			$qb->join('o.items', 'i');
		}

		switch ($owner) {
			case self::OWNER_MY:
				$user = $this->tokenStorage->getToken()->getUser();
				$qb->where('o.librarian=' . $user->getId());
				break;
		}

		switch ($state) {
			case self::STATE_ACTIVE:
				$qb->where('i.closed = 0');
				break;
			case self::STATE_CLOSED:
				$qb->where('i.closed = 1');
				break;
		}

		switch($expired) {
			case self::EXPIRED_EXPIRED:
				$qb->where('i.expiredAt <= :date');
				$qb->setParameter('date', (new \DateTime())->format('Y-m-d'));
				break;
		}

		return $qb->getQuery();
	}

	/**
	 * @param Order $order
	 *
	 * @return void
	 */
	public function deleteOrder(Order $order)
	{
		$this->_em->remove($order);
		$this->_em->flush();
	}

	/**
	 * @param Order $order
	 *
	 * @return void
	 */
	public function saveOrder(Order $order)
	{
		$this->_em->persist($order);
		$this->_em->flush();
	}
}
