<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Order;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\Query;

/**
 * Interface OrderRepositoryInterface
 * @package AppBundle\Repository
 */
interface OrderRepositoryInterface extends ObjectRepository
{

	const OWNER_ALL = 'all';
	const OWNER_MY = 'my';

	const STATE_ALL = 'all';
	const STATE_ACTIVE = 'active';
	const STATE_CLOSED = 'closed';

	const EXPIRED_ALL = 'all';
	const EXPIRED_EXPIRED = 'expired';

	/**
	 * @param $owner
	 * @param $state
	 * @param $expired
	 *
	 * @return Query
	 */
	public function findLibrarianFilteredOrders($owner, $state, $expired);

	/**
	 * @param Order $order
	 *
	 * @return void
	 */
	public function deleteOrder(Order $order);

	/**
	 * @param Order $order
	 *
	 * @return void
	 */
	public function saveOrder(Order $order);

}