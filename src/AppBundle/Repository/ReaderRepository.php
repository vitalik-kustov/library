<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Reader;
use Doctrine\DBAL\Types\DateType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class ReaderRepository
 * @package AppBundle\Repository
 */
class ReaderRepository extends EntityRepository implements ReaderRepositoryInterface
{
	/**
	 * @return Query
	 */
	public function findDeptorsWithDebtCount()
	{
		$qb = $this->_em->createQueryBuilder('reader')
			->select('reader as user, count(i.id) as expiredItemsCount')
			->from('AppBundle:Reader', 'reader')
			->leftJoin('reader.orders', 'orders')
			->leftJoin('orders.items', 'i', 'with', 'i.closed=0')
			->groupBy('reader.id')
			->orderBy('expiredItemsCount', 'DESC');

		$qb->where(
			$qb->expr()
				->lt('i.expiredAt', ':date')
		);
		$qb->setParameter('date', new \DateTime(), DateType::DATE);

		return $qb->getQuery();
	}

}
