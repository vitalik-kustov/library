<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Reader;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\Query;

/**
 * Interface ReaderRepositoryInterface
 * @package AppBundle\Repository
 */
interface ReaderRepositoryInterface extends ObjectRepository
{

	/**
	 * @return Query
	 */
	public function findDeptorsWithDebtCount();


}