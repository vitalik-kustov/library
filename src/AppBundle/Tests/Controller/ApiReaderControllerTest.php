<?php
/**
 * Created by PhpStorm.
 * User: Виталий
 * Date: 05.06.2015
 * Time: 1:25
 */

namespace AppBundle\Tests\Controller;


use AppBundle\Tests\WebTestCase;

class ApiReaderControllerTest extends WebTestCase
{


	public function setUp()
	{
		$fixtures = array(
			'AppBundle\DataFixtures\ORM\LoadBookAuthorsData',
			'AppBundle\DataFixtures\ORM\LoadUsersData',
			'AppBundle\DataFixtures\ORM\LoadOrdersData',
		);
		$this->loadFixtures($fixtures);
	}

	public function testReaders()
	{
		$this->userRoleApiCanGetAndPostReaders();
		$this->userRoleLibrarianCanGetAndCannotPostReaders();
		$this->userRoleReaderCannotGetReaders();
	}

	private function userRoleApiCanGetAndPostReaders()
	{
		$client = $this->createAuthenticatedClient('admin', 'admin');
		$client->request('GET', $this->getUrl('api_1_get_readers'), ['limit' => 2]);
		$response = $client->getResponse();
		$this->assertJsonResponse($response, 200);
		$data = json_decode($response->getContent(), true);
		static::assertEquals($this->getExpectedData(), $data);

		$client->request(
			'POST',
			$this->getUrl('api_1_post_reader', ['_format' => 'json']),
			[
				'username' => 'testReader',
				'email' => 'testreader@asd.as',
				'password' => 'asd'
			]
		);
		$response = $client->getResponse();
		static::assertEquals(201, $response->getStatusCode());
	}

	private function getExpectedData()
	{
		return [
			'limit' => 2,
			'offset' => 0,
			'readers' => [

				[
					'username' => 'test1',
					'email' => 'test1@test.com',
					'id' => 1
				],
				[
					'username' => 'test2',
					'email' => 'test2@test.com',
					'id' => 2
				]
			]
		];
	}

	private function userRoleLibrarianCanGetAndCannotPostReaders()
	{
		// test (ROLE_LIBRARIAN)
		$client = $this->createAuthenticatedClient('moder', 'moder');
		$client->request('GET', $this->getUrl('api_1_get_readers'), ['limit' => 2]);
		$response = $client->getResponse();
		$this->assertJsonResponse($response, 200);
		$data = json_decode($response->getContent(), true);
		static::assertEquals($this->getExpectedData(), $data);

		$client->request(
			'POST',
			$this->getUrl('api_1_post_reader', ['_format' => 'json']),
			[
				'username' => 'testReader',
				'email' => 'testreader@asd.as',
				'password' => 'asd'
			]
		);
		$response = $client->getResponse();
		static::assertEquals(403, $response->getStatusCode());
	}

	private function userRoleReaderCannotGetReaders()
	{
		// test reader client (ROLE_READER)
		$readerClient = $this->createAuthenticatedClient('test1', 'test1');
		$readerClient->request('GET', $this->getUrl('api_1_get_readers'));
		$response = $readerClient->getResponse();
		$this->assertJsonResponse($response, 403);
	}
}