<?php

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class InFuture
 * @Annotation
 * @package AppBundle\Validator\Constraints
 */
class InFuture extends Constraint
{

	public $message = 'The date "%date%" must be in future';

	public function validatedBy()
	{
		return 'in_future';
	}
}