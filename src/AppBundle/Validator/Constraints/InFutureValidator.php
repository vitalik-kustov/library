<?php

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class InFutureValidator
 * @package AppBundle\Validator\Constraints
 */
class InFutureValidator extends ConstraintValidator
{

	/**
	 * Checks if the passed value is valid.
	 *
	 * @param mixed $value The value that should be validated
	 * @param Constraint $constraint The constraint for the validation
	 *
	 * @api
	 */
	public function validate($value, Constraint $constraint)
	{
		if (!$constraint instanceof InFuture) {
			throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\InFuture');
		}
		if (!$value instanceof \DateTime) {
			$value = new \DateTime($value);
		}
		if ($value < new \DateTime()) {
			$this->context->buildViolation($constraint->message)
				->setParameter('%date%', $value->format('d.m.Y'))
				->addViolation();
		}
	}
}