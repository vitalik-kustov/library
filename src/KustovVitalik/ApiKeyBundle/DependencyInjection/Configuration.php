<?php

namespace KustovVitalik\ApiKeyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('kustov_vitalik_api_key');

        $rootNode
            ->children()
                ->scalarNode('api_key_transport')
                    ->defaultValue('header')
                    ->validate()
                        ->ifNotInArray(['query', 'header'])
                        ->thenInvalid('Unknown authentication api key transport mechanism "%s".')
                    ->end()
                ->end()
                ->scalarNode('api_key_parameter_name')
                    ->defaultValue('X-OAuth-Token')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
