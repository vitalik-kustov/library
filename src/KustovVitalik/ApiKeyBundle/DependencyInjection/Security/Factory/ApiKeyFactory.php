<?php
/**
 * Created by PhpStorm.
 * Date: 18.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\DependencyInjection\Security\Factory;


use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * {@inheritDoc}
 */
class ApiKeyFactory implements SecurityFactoryInterface
{

    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'kustov_vitalik.api_key.security.authentication.provider.api_key.'.$id;
        $container->setDefinition(
            $providerId,
            new DefinitionDecorator('kustov_vitalik.api_key.security.authentication.provider.api_key')
        )
            ->addMethodCall('setUserProvider', [new Reference($userProvider)]);

        $listenerId = 'kustov_vitalik.api_key.security.firewall.api_key_listener.'.$id;
        $container->setDefinition(
            $listenerId,
            new DefinitionDecorator('kustov_vitalik.api_key.security.firewall.api_key_listener')
        );

        return [$providerId, $listenerId, $defaultEntryPoint];
    }

    /**
     * Defines the position at which the provider is called.
     * Possible values: pre_auth, form, http, and remember_me.
     * @return string
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    public function getKey()
    {
        return 'api_key';
    }

    public function addConfiguration(NodeDefinition $builder)
    {

    }
}