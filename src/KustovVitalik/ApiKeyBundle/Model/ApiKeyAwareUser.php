<?php
/**
 * Created by PhpStorm.
 * Date: 18.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\Model;


use Symfony\Component\Security\Core\User\UserInterface;

interface ApiKeyAwareUser
{
    /**
     * @return string|null
     */
    public function getApiKey();

    /**
     * @param $apiKey
     *
     * @return UserInterface|null
     */
    public function setApiKey($apiKey);
}