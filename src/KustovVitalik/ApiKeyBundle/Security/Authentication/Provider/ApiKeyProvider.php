<?php
/**
 * Created by PhpStorm.
 * Date: 17.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\Security\Authentication\Provider;


use KustovVitalik\ApiKeyBundle\Security\Authentication\Token\ApiKeyToken;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\ChainUserProvider;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Authentication provider for Api Key.
 * @author Vitaly Kustov <kustov.vitalik@gmail.com>
 */
class ApiKeyProvider implements AuthenticationProviderInterface
{

    /**
     * @var UserProviderInterface
     */
    private $userProvider;

    /**
     * Attempts to authenticate a TokenInterface object.
     *
     * @param TokenInterface $token The TokenInterface instance to authenticate
     *
     * @return TokenInterface An authenticated TokenInterface instance, never null
     * @throws AuthenticationException if the authentication fails
     */
    public function authenticate(TokenInterface $token)
    {
        if ($this->userProvider instanceof ChainUserProvider) {
            foreach ($this->userProvider->getProviders() as $provider) {
                if ($provider instanceof ApiKeyUserProvider) {
                    return $this->doAuth($provider, $token);
                }
            }

        } else {
            if ($this->userProvider instanceof ApiKeyUserProvider) {
                return $this->doAuth($this->userProvider, $token);
            }
        }
    }

    /**
     * Checks whether this provider supports the given token.
     *
     * @param TokenInterface $token A TokenInterface instance
     *
     * @return bool true if the implementation supports the Token, false otherwise
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof ApiKeyToken;
    }

    /**
     * @param ApiKeyUserProvider $userProvider
     * @param TokenInterface $token
     *
     * @return ApiKeyToken
     * @throws AuthenticationException
     */
    private function doAuth(ApiKeyUserProvider $userProvider, TokenInterface $token)
    {
        $user = $userProvider->loadUserByApiKey($token->getCredentials());
        if ($user) {
            $authenticatedToken = new ApiKeyToken($user->getRoles());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }

        throw new AuthenticationException();
    }

    /**
     * @param UserProviderInterface $userProvider
     */
    public function setUserProvider(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }


}