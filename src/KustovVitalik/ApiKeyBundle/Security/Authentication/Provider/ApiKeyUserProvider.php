<?php
/**
 * Created by PhpStorm.
 * Date: 17.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\Security\Authentication\Provider;


use Symfony\Component\Security\Core\User\UserInterface;

interface ApiKeyUserProvider
{
    /**
     * @param $apiKey
     *
     * @return UserInterface
     */
    public function loadUserByApiKey($apiKey);
}