<?php
/**
 * Created by PhpStorm.
 * Date: 17.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\Security\Authentication\Token;


use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Class for Api Key Token
 * @author Vitaly Kustov <kustov.vitalik@gmail.com>
 */
class ApiKeyToken extends AbstractToken
{

    /**
     * @var string
     */
    private $apiKey;

    /**
     * Constructor.
     *
     * @param RoleInterface[] $roles An array of roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);
        $this->setAuthenticated(count($roles) > 0);
    }


    /**
     * Returns the user credentials.
     * @return mixed The user credentials
     */
    public function getCredentials()
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }


}