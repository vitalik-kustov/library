<?php
/**
 * Created by PhpStorm.
 * Date: 17.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\Security\Extractor;


use Symfony\Component\HttpFoundation\Request;

interface ApiKeyExtractor
{
    /**
     * @param Request $request
     *
     * @return bool
     */
    public function hasKey(Request $request);

    /**
     * @param Request $request
     *
     * @return string
     */
    public function getKay(Request $request);
}