<?php
/**
 * Created by PhpStorm.
 * Date: 17.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\Security\Extractor;


use Symfony\Component\HttpFoundation\Request;

class HttpHeaderApiKeyExtractor implements ApiKeyExtractor
{
    /**
     * @var string
     */
    private $apiKeyParameterName;

    /**
     * HttpHeaderApiKeyExtractor constructor.
     *
     * @param string $apiKeyParameterName
     */
    public function __construct($apiKeyParameterName)
    {
        $this->apiKeyParameterName = $apiKeyParameterName;
    }


    /**
     * @param Request $request
     *
     * @return bool
     */
    public function hasKey(Request $request)
    {
        return $request->headers->has($this->apiKeyParameterName);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function getKay(Request $request)
    {
        return $request->headers->get($this->apiKeyParameterName);
    }
}