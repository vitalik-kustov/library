<?php
/**
 * Created by PhpStorm.
 * Date: 17.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ApiKeyBundle\Security\Firewall;


use KustovVitalik\ApiKeyBundle\Security\Authentication\Token\ApiKeyToken;
use KustovVitalik\ApiKeyBundle\Security\Extractor\ApiKeyExtractor;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

class ApiKeyListener implements ListenerInterface
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationManager;

    /**
     * @var ApiKeyExtractor
     */
    private $apiKeyExtractor;

    /**
     * ApiKeyListener constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     * @param ApiKeyExtractor $apiKeyExtractor
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthenticationManagerInterface $authenticationManager,
        ApiKeyExtractor $apiKeyExtractor
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->apiKeyExtractor = $apiKeyExtractor;
    }


    /**
     * This interface must be implemented by firewall listeners.
     *
     * @param GetResponseEvent $event
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$this->apiKeyExtractor->hasKey($request)) {
            $event->setResponse(new Response('', Response::HTTP_UNAUTHORIZED));
            return;
        }

        $apiKey = $this->apiKeyExtractor->getKay($request);

        $token = new ApiKeyToken();
        $token->setApiKey($apiKey);

        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);
        } catch (AuthenticationException $e) {
            $token = $this->tokenStorage->getToken();
            if ($token instanceof ApiKeyToken && $token->getCredentials() === $apiKey) {
                $this->tokenStorage->setToken(null);
            }

            $response = new Response($e->getMessage(), Response::HTTP_FORBIDDEN);
            $event->setResponse($response);
        }
    }
}