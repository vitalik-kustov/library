<?php
/**
 * Created by PhpStorm.
 * Date: 10.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ServiceEntityListenerBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\BadMethodCallException;
use Symfony\Component\DependencyInjection\Exception\InactiveScopeException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * {@inheritDoc}
 */
class DoctrineEntityListenerCompilerPass implements CompilerPassInterface
{

	/**
	 * You can modify the container here before it is dumped to PHP code.
	 *
	 * @param ContainerBuilder $container
	 *
	 * @throws InvalidArgumentException
	 * @throws BadMethodCallException
	 * @throws InactiveScopeException
	 * @throws LogicException
	 * @throws \Exception
	 *
	 * @api
	 */
	public function process(ContainerBuilder $container)
	{
		$ems = $container->getParameter('doctrine.entity_managers');
		foreach ($ems as $name => $em) {
			$container->getDefinition(sprintf('doctrine.orm.%s_configuration', $name))
				->addMethodCall('setEntityListenerResolver', [new Reference('kustov_vitalik.doctrine.entity_listener_resolver')]);
		}

		$definishion = $container->getDefinition('kustov_vitalik.doctrine.entity_listener_resolver');
		$services = $container->findTaggedServiceIds('doctrine.entity_listener');
		foreach ($services as $service => $attributes) {
			$definishion->addMethodCall(
				'addMapping',
				[$container->getDefinition($service)->getClass(), $service]
			);
		}
	}
}