<?php
/**
 * Created by PhpStorm.
 * Date: 10.07.2015
 * @author Vitaly
 */

namespace KustovVitalik\ServiceEntityListenerBundle\Doctrine;


use Doctrine\ORM\Mapping\DefaultEntityListenerResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class DoctrineEntityListenerResolver extends DefaultEntityListenerResolver
{
	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * @var array
	 */
	private $mapping;

	/**
	 * DoctrineEntityListenerResolver constructor.
	 *
	 * @param ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	/**
	 * @param string $className
	 * @param string $service
	 */
	public function addMapping($className, $service)
	{
		$this->mapping[$className] = $service;
	}

	/**
	 * Returns a entity listener instance for the given class name.
	 *
	 * @param string $className The fully-qualified class name
	 *
	 * @return object An entity listener
	 *
	 * @throws ServiceCircularReferenceException
	 * @throws ServiceNotFoundException
	 */
	public function resolve($className)
	{
		if (array_key_exists($className, $this->mapping) && $this->container->has($this->mapping[$className])) {
			return $this->container->get($this->mapping[$className]);
		}

		return parent::resolve($className);
	}


}