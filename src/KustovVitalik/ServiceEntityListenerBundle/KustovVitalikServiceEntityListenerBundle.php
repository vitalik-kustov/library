<?php

namespace KustovVitalik\ServiceEntityListenerBundle;

use KustovVitalik\ServiceEntityListenerBundle\DependencyInjection\Compiler\DoctrineEntityListenerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * An implementation of BundleInterface that adds a few conventions
 * for DependencyInjection extensions.
 * @author Vitaly Kustov <kustov.vitalik@gmail.com>
 * @api
 */
class KustovVitalikServiceEntityListenerBundle extends Bundle
{
	/**
	 * Builds the bundle.
	 * It is only ever called once when the cache is empty.
	 * This method can be overridden to register compilation passes,
	 * other extensions, ...
	 *
	 * @param ContainerBuilder $container A ContainerBuilder instance
	 */
	public function build(ContainerBuilder $container)
	{
		parent::build($container);

		$container->addCompilerPass(new DoctrineEntityListenerCompilerPass());
	}

}
